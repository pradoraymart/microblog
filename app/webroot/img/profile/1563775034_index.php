<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Simple Ajax</title>
  </head>
  <body>
    <h1>User List</h1>
    <?php
    require_once('db.php');
    $content = '';
    $sql = 'select * from users';
    $result = $db->query($sql);

    if ($result->num_rows > 0) {
        $content = '<table class="table">';
        $content .= '<thead><tr><th scope="col">ID</th><th scope="col">Username</th></tr></thead>';
        $tbody = '<tbody>';
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $tbody .= '<tr><td>' . $row["id"] . '</td><td>' . $row["username"] . '</td></tr>';
        }
        $tbody .= '</tbody>';
        $content .= $tbody;
        $content .= '</table>';
    } else {
        $content .= '0 results';
    }
    $db->close();
    echo $content;
    ?>
    <a class="add_user btn btn-primary" href="add.php">Add User</a>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div>
      </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(function() {
            $('body').on('click', '.add_user', function(e){
                //prevents redirection
                e.preventDefault();
                var href = $(this).attr('href'),
                    className = $(this).attr('class').split(' ')[0]; //first class name of the element
                    form = $(this).closest('form'),
                    action = $(form).attr('action');

                //check if button is not from Form
                if (href !== undefined) {
                    //create get request
                    $request = $.get(href);
                } else {
                    //create post request
                    $request = $.post(action, $(form).serialize(), {'format': 'json'});
                }

                $request.done(function(data){
                    console.log(data);
                    //check if button is not from Form
                    if (href !== undefined) {
                        //display form

                        //change modal label depending on purpose
                        if (className === 'add_user') {
                            $('#exampleModalLabel').html('ADD USER');
                        }

                        //append form
                        $('#exampleModal').find('.modal-body').html(data);
                        //show modal
                        $('#exampleModal').modal('show');
                    } else {
                        //append post result

                        //parse data to json format
                        data = JSON.parse(data);

                        //check if successful
                        if (data.hasOwnProperty('result')) {
                            if (className === 'add_user') {
                                var newRow = '<tr><td>' +
                                    data.result.id +
                                    '</td><td>' +
                                    data.result.username +
                                    '</td></tr>';

                                $('table').find('tbody').append(newRow);
                            }

                            $('#exampleModalLabel').html('');
                            $('#exampleModal').find('.modal-body').html('');
                            $('#exampleModal').modal('hide');
                        } else {
                            alert(data.message);
                        }
                    }
                })
                .fail(function(data){
                    console.log('request failed');
                });
            });
        });
    </script>
  </body>
</html>