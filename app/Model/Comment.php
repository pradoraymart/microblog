<?php
App::uses('AppModel', 'Model');

class Comment extends AppModel
{
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ],
        'Post' => [
            'className' => 'Post',
            'foreignKey' => 'post_id'
        ]
    ];

    public $validate = [
        'content' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'Please insert a comment first'
            ],
            'max_length' => [
                'rule' => ['maxLength', '140'],
                'message' => 'Comment must not exceed to 140 characters',
            ]
        ]
    ];
}
