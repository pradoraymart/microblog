<?php
App::uses('AppModel', 'Model');

class Like extends AppModel
{
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ],
        'Post' => [
            'className' => 'Post',
            'foreignKey' => 'post_id'
        ]
    ];
}
