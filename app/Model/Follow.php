<?php
App::uses('AppModel', 'Model');

class Follow extends AppModel
{
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id'
        ]
    ];
}
