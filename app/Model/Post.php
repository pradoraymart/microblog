<?php
App::uses('AppModel', 'Model');

class Post extends AppModel
{
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id',
            'fields' => ['id', 'first_name', 'last_name', 'image']
        ],
        'Retweet' => [
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'fields' => ['id', 'user_id', 'content', 'created', 'modified', 'is_deleted']
        ],
        'AuthorRetweet' => [
            'className' => 'User',
            'foreignKey' => 'author_id',
            'fields' => ['id', 'first_name', 'last_name', 'image']
        ]
    ];

    public $hasMany = [
        'Comment' => [
            'className' => 'Comment',
              'finderQuery' => 'SELECT `Comment`.`id`, `Comment`.`post_id`, `Comment`.`user_id`,`Comment`.`content`,`Comment`.`created`, `User`.`id`, `User`.`first_name`,`User`.`last_name`,`User`.`image`
                FROM `microblog_raymart`.`comments` AS `Comment` LEFT JOIN `microblog_raymart`.`users` AS `User` ON
                (`Comment`.`user_id` = `User`.`id`) WHERE `Comment`.`post_id` = {$__cakeID__$}',
            'limit' => '3',
            'order' => 'Comment.created DESC',
            'dependent' => true
        ],
        'Like' => [
            'className' => 'Like',
            'foreignKey' => 'post_id',
            'conditions' => ['Like.status' => 1]
        ],
        'NoOfRetweets' => [
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'conditions' => [
                'NoOfRetweets.post_id' => 'Post.id',
                'NoOfRetweets.is_deleted' => 0
            ],
            'fields' => ['id','post_id','is_deleted']
        ]
    ];

    public $validate = [
        'content' => [
            'max_length' => [
                'rule' => ['maxLength', '140'],
                'message' => 'Tweet must not exceed to 140 characters',
                'allowEmpty' => true
            ]
        ],
        'image' => [
            'rule1' => [
                'rule' => [
                    'extension',
                    ['gif', 'jpeg', 'png', 'jpg']
                ],
                'message' => 'Please supply a valid image.',
                'allowEmpty' => true
            ],
            'rule2' => [
                'rule' => ['fileSize', '<=', '5MB'],
                'message' => 'Image must be less than 5MB'
            ]
        ]
    ];
}
