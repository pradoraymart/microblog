<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel
{
    public $hasMany = [
        'Post' => [
            'className' => 'Post',
            'foreignKey' => 'user_id'
        ],
        'Follow' => [
            'className' => 'Follow',
            'foreignKey' => 'user_id'
        ],
        'AuthorRetweet' => [
            'className' => 'Post',
            'foreignKey' => 'author_id'
        ],
        'Comment' => [
            'className' => 'Comment',
            'foreignKey' => 'user_id'
        ]
    ];

    public $validate = [
        'first_name' => [
            'nonEmpty' => [
                'rule' => ['notBlank'],
                'message' => 'First name is required',
                'allowEmpty' => false
            ],
            'alphaNumeric' => [
                'rule'    => '/^[a-zA-Z\.\s]*$/i',
                'message' => 'Please enter letter characters only'
            ],
            'max_length' => [
                'rule' => ['maxLength', '50'],
                'message' => 'Names must not be more than 50 characters'
            ]
        ],
        'last_name' => [
            'nonEmpty' => [
                'rule' => ['notBlank'],
                'message' => 'Last name is required',
                'allowEmpty' => false
            ],
            'alphaNumeric' => [
                'rule'    => '/^[a-zA-Z\.\s]*$/i',
                'message' => 'Please enter letter characters only'
            ],
            'max_length' => [
                'rule' => ['maxLength', '50'],
                'message' => 'Names must not be more than 50 characters'
            ]
        ],
        'username' => [
            'nonEmpty' => [
                'rule' => ['notBlank'],
                'message' => 'A username is required',
                'allowEmpty' => false
            ],
            'between' => [
                'rule' => ['between', 5, 15],
                'required' => true,
                'message' => 'Usernames must be between 5 to 15 characters'
            ],
            'unique' => [
                'rule'    => ['isUnique'],
                'message' => 'This username is already in use'
            ],
            'alphaNumeric' => [
                'rule'    => '/^[a-z0-9]{3,}$/i',
                'message' => 'Username can only be letters and numbers'
            ]
        ],
        'email' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'An email is required'
            ],
            'unique' => [
                'rule'    => 'isUnique',
                'message' => 'This email is already in use',
            ]
        ],
        'password' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'A password is required'
            ],
            'min_length' => [
                'rule' => ['minLength', '8'],
                'message' => 'Password must have a mimimum of 8 characters'
            ]
        ],
        'confirm_password' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'A password is required'
            ],
            'password_confirm' => [
                'rule' => ['password_confirm'],
                'message' => 'Password do not match'
            ]
        ],
        'gender' => [
            'required' => [
                'rule' => 'notBlank',
                'message' => 'Please choose a gender'
            ],
            'allowedChoice' => [
                'rule' => ['inList', [0, 1]],
                'message' => 'Please choose either Male or Female only'
            ]
        ],
        'date_of_birth' => [
            'rule' =>  ['checkOver14', 'date' , 'mdy'],
            'message' => 'You must be 14 years old and above to register'
        ],
        'self_description' => [
            'max_length' => [
                'rule' => ['maxLength', '50'],
                'message' => 'Bio must not be more than 50 characters',
                'allowEmpty' => true
            ]
        ],
        'image' => [
            'rule1' => [
                'rule' => [
                    'extension',
                    ['gif', 'jpeg', 'png', 'jpg']
                ],
                'message' => 'Please supply a valid image.',
                'allowEmpty' => true
            ],
            'rule2' => [
                'rule' => ['fileSize', '<=', '20MB'],
                'message' => 'Image must be less than 20MB'
            ]
        ]
    ];

    /**
     * Validates user password if both fields are matched
     *
     * @return boolean true to show validation error if user password doesnt matched
     */
    public function password_confirm()
    {
        //check user password field and confirm password if match
        if (($this->data['User']['password'] !== $this->data['User']['confirm_password'])) {
            return false;
        }
        return true;
    }

    /**
     * Check user age if above 18 years old
     *
     * @param boolean $check
     * @return boolean true to show validation error if user is not above 18 years old
     */
    public function checkOver14($check) {
        $bday = strtotime($check['date_of_birth']);
        //
        if (time() < strtotime('+14 years', $bday)) {
            return false;
        }
        return true;
      }

    /**
     * Hash user password using BlowfishPasswordHasher
     *
     * @param string $options
     * @return bool true if sucessfully hashed
     */
    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            //create the class object
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
}
