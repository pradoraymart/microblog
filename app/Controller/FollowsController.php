<?php
App::uses('AppController', 'Controller');

/**
 * Follows Controller
 *
 * @property App\Model\Follow
 */
class FollowsController extends AppController
{
    public $uses = ['Post', 'User', 'Follow'];
    public $components = ['Flash', 'Paginator', 'Hashids'];

    /**
     * Follow other users
     *
     * @param int $followerId
     * @return void
     */
    public function follow($followId)
    {
        // decode hashed id
        $followerId = $this->Hashids->decode($followId);
        // get current logged in user
        $id = $this->Auth->user('id');

        // avoid user to follow his/her own account
        if ($followerId != $id) {
        // avoid following the already followed user
            $checkFollow = $this->Follow->find('first', [
                'conditions' => [
                    'AND' => [
                        'user_id' => $id,
                        'following' => $followerId]
                    ]
                ]
            );
            if (!$checkFollow) {
                // set data to be saved
                $data = [
                    'Follow' => [
                        'user_id' => $id,
                        'following' => $followerId
                        ]
                    ];

                if ($this->Follow->save($data)) {
                    $this->Flash->success(__('Successfully followed user'));

                    return $this->redirect(array('controller' => 'users', 'action' => "view/$followId"));
                }
                $this->Flash->error(__('Error following user, Please try again'));
            }
            $this->Flash->error(__('Already following the user'));
            return $this->redirect(array('controller' => 'users', 'action' => "view/$followId"));
        } else {
            $this->Flash->error(__('Method not allowed, you cant follow/unfollow yourself'));
        }
        return $this->redirect(array('controller' => 'users', 'action' => "view/$followId"));
    }

    /**
     * Unfollow other users
     *
     * @param int $followerId
     * @return void
     */
    public function unfollow($followId)
    {
        // decode hashed id
        $followerId = $this->Hashids->decode($followId);
        // get current logged in user
        $id = $this->Auth->user('id');

        // avoid user to follow his/her own account
        if ($followerId != $id) {
        // avoid following the already followed user
            $checkFollow = $this->Follow->find('first', [
                'conditions' => [
                    'AND' => [
                        'user_id' => $id,
                        'following' => $followerId]
                    ]
                ]
            );

            // check the record is still exists else delete the record
            if ($checkFollow) {
                // unfollow user by deleting record on Follows table
                if ($this->Follow->deleteAll(array('user_id' => $id, 'following' => $followerId))) {
                    $this->Flash->success(__('Successfully unfollowed user'));

                    return $this->redirect(array('controller' => 'users', 'action' => "view/$followId"));
                }
                $this->Flash->error(__('Error unfollowing user, Please try again'));
            }
            $this->Flash->error(__('Already unfollowed the user'));
        } else {
            $this->Flash->error(__('Method not allowed, you cant follow/unfollow yourself'));
        }
        return $this->redirect(array('controller' => 'users', 'action' => "view/$followId"));
    }
}
