<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::import('Sanitize');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = [
        'Hashids' => [
            'salt' => '0R9A4Y5M5A2R6T1627', // Required
            'min_hash_length' => 8, // Optional
            'alphabet' => '', // Optional
        ],
        'RequestHandler',
        'Flash',
        'Session',
        'Auth' => [
            'loginAction' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'posts',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password'],
                    'scope' => ['is_verified' => 1],
                    'userModel' => 'User',
                    'passwordHasher' => 'Blowfish'
                ]
            ],
            'authError' => 'You must be logged in to view this page.',
            'loginError' => 'Invalid Username or Password entered, please try again.'
        ]
    ];

    public function beforeFilter() {
        $this->Auth->allow('login');
        $this->set('authUser', $this->Auth->user());
        // remove leading and trailing whitespace from posted data
        if (!function_exists('trimItem')) {
            function trimItem(&$item,$key){
                if (is_string($item)){
                    $item = trim($item);
                }
            }
        }
        array_walk_recursive($this->request->data, 'trimItem');
    }
}
