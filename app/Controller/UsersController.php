<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Short description of the class.
 *
 * @deprecated 3.0.0 Deprecated in 2.6.0. Will be removed in 3.0.0. Use Bar instead.
 * @see Bar
 * @link https://book.cakephp.org/2.0/en/foo.html
 */
class UsersController extends AppController
{
    public $uses = ['User', 'Follow', 'Comment', 'Post'];
    public $recursive = 2;
    public $components = ['Flash', 'Paginator', 'Hashids'];

    /**
     * This function is executed before every action in the controller
     * Allows user to add, edit and delete posts
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register, logout and edit profile
        $this->Auth->allow('register', 'verification', 'edit');
    }

    /**
     * Search user and posts
     * Display list of post and user based on sent keyword
     * @return void
     */
    public function search()
    {
        //get the search value from the url
        $keyword = $this->params['url']['search'];
        if (!empty($keyword)) {
            // fetch searched user
            $this->Paginator->settings = [
                'User' => [
                    'limit' => 4,
                    'order' => ['User.first_name' => 'asc'],
                    'conditions' => [
                        'AND' => ['User.is_verified' => 1,
                            'OR' => [
                                'User.first_name LIKE' => "%" . trim($keyword) . "%",
                                'User.last_name LIKE' => "%" . trim($keyword) . "%",
                                'User.username LIKE' => "%" . trim($keyword) . "%",
                            ]
                        ]
                    ]
                ]
            ];
            $userResult = $this->Paginator->paginate('User');
            $this->set('user', $userResult);

            // fetch search posts
            $this->paginate = [
                'conditions' => [
                    'OR' => [
                        ['Post.content LIKE' => "%" . trim($keyword) . "%"],
                        ['Post.retweet_content LIKE' => "%" . trim($keyword) . "%"],
                    ],
                    ['Post.is_deleted' => 0]
                ],
                'limit' => 4,
                'order' => ['Post.created' => 'desc'],
                'group' => ['Post.id']
            ];
            $posts = $this->paginate($this->User->Post);
            $this->set('posts', $posts);

            $authUser = $this->User->find('all', [
                'conditions' => ['User.id' => $this->Auth->user('id')]]);

            $this->set('authUser', $authUser);
            $this->Session->write('redirect', $this->request->here());
        } else {
            //show redirect to page if user did not insert text on search input
            $this->redirect($this->referer());
        }
    }

    /**
     * Edit user accout information
     *
     * @param int $id // Handles request user id
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function edit()
    {
        $id = $this->Auth->user('id');
        // prevent user to edit profile if not logged in
        if (!$id) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        $user = $this->User->findById($id);
        //show NotFoundException if user id not exists
        if (!$user) {
            throw new NotFoundException();
        }

        //encode id to redirect on user profile view
        $userId = $this->Hashids->encode($id);

        if ($this->request->is(['post','put'])) {
            $filename = '';
            //check user uploaded image
            if (!empty($this->request->data['User']['image']['name'])) {

                $uploadData = $this->request->data['User']['image'];
                $filename = basename($uploadData['name']);
                $uploadFolder = WWW_ROOT. 'img' ;
                $filename = time() .'_'. $filename;
                $uploadPath =  $uploadFolder . DS . 'profile' . DS .  $filename;

                if (!move_uploaded_file($uploadData['tmp_name'], $uploadPath)) {
                    $this->Flash->error(__('Error Uploading Image, Please try again'));
                }

            $this->request->data['User']['image'] = $filename;
            } else {
                unset( $this->request->data['User']['image'] );
                $this->User->validator()->remove('image');
            }

            //check if user change gender
            $this->User->id = $id;
            $oldGender = $this->User->field('gender');
            $gender = $this->request->data['User']['gender'];
            $checkGender = ($oldGender == $gender ? true : false);
            if ($checkGender == false) {
                if (empty($this->request->data['User']['image'])) {
                    $imageName = $this->User->field('image');
                    if ($imageName == 'female_user.png' || $imageName == 'male_user.png') {
                        switch ($gender) {
                            case 1:
                                $this->request->data['User']['image'] = 'female_user.png';
                                break;
                            case 0:
                                $this->request->data['User']['image'] = 'male_user.png';
                                break;
                        }
                    }
                }
            }

            //check if user inserted bio
            if (!empty($this->request->data['User']['self_description'])) {
                $this->request->data['User']['self_description'];
            } else if (!empty($this->User->field('self_description'))) { //if user save blank data
                $this->request->data['self_description'] = null;
            }

            // check password if not empty
            if (!empty($new_password = $this->request->data['User']['new_password'])) {
                $this->request->data['User']['password'] = $new_password;
            } else { // else, we remove the rules on password
                $this->User->validator()->remove('password');
                $this->User->validator()->remove('confirm_password');
            }

            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Successfully Updated'));

                return $this->redirect(array('controller' => 'users', 'action' => "view/$userId"));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
            unset($this->User->validate['confirm_password']);
        }

        if (!$this->request->data) {
            $this->request->data = $user;
        }

        $this->set('user', $user);
    }

    /**
     * View user accout information
     *
     * @param int $id // Handles request user id
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function view($id = null)
    {
        //decode hash passed id
        $id = $this->Hashids->decode($id);
        if (!$id) {
            throw new NotFoundException();
        }

        //display user own posts
        $this->Paginator->settings = [
            'Post' => [
                'limit' => 4,
                'order' => ['Post.created' => 'desc'],
                'conditions' => [
                    'AND' => [
                        ['Post.user_id' => $id],
                        ['Post.is_deleted' => 0] // only undeleted posts
                    ]
                ]
            ]
        ];
        $posts = $this->Paginator->Paginate('Post');
        $this->set('posts', $posts);

        //find user
        $user = $this->User->find('all', [
            'conditions' => array('User.id' => $id)]);

        // get and pass the current logged in user credentials
        $userAuth = $this->User->find('all', [
            'conditions' => ['User.id' => $this->Auth->user('id')]]);

        // For Follow Button status
        $follow = $this->Follow->find('all', [
            'conditions' => [
                'Follow.following' => $id,
                'Follow.user_id' => $this->Auth->user('id')
                ]
            ]);

        // Get Following
        $followers = $this->User->find('all', [
            'conditions' => ['followJoin.user_id' => $id],
            'joins' => [
                ['table' => 'follows',
                'alias' => 'followJoin',
                'type' => 'INNER',
                'conditions' => ['User.id = followJoin.following']]
            ]
        ]);

        // Get Followers
        $following = $this->User->find('all', [
            'conditions' => ['followJoin.following' => $id],
            'joins' => [
                ['table' => 'follows',
                'alias' => 'followJoin',
                'type' => 'INNER',
                'conditions' => ['User.id = followJoin.user_id']]
            ]
        ]);

        if (!$user) {
            throw new NotFoundException(__('User not Found'));
        }

        $this->Session->write('redirect', $this->request->here());
        $this->set(compact('user', 'userAuth', 'follow', 'following', 'followers'));
    }

    /**
     * User registration
     *
     * @param int $id // Handles request user id
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function register()
    {
        if (!$this->Auth->user()) {
        //check if the request is post
            if ($this->request->is('post')) {
                $this->User->create();

                $emailTo = $this->request->data['User']['email']; // get recipient email for email verification
                $token = Security::hash(Security::randomBytes(32)); // generate token for secure verification

                //remove validation for image on registration
                unset($this->User->validate['image']);
                //set user profile image based on gender
                $this->request->data['User']['image'] = 'male_user.png';
                if ($this->request->data['User']['gender'] == '1') {
                    $this->request->data['User']['image'] = 'female_user.png';
                }

                if ($this->User->save($this->request->data)) {
                    $this->User->id;
                    $this->User->saveField('token', $token); //save token
                    $this->Flash->success(__('You are almost there! An email verification link has been sent to ' .$emailTo. ' .Please click the link to verify'));

                    //send email using cakephp Email
                    $Email = new CakeEmail('default');
                    $Email->template('emailVerification') //sent this view to email
                        ->emailFormat('html')
                        ->subject('Confirm Registration')
                        ->to($emailTo)
                        ->viewVars(array('token' => $token,'id' => $this->User->id)) //pass data to email_verification.ctp
                        ->send();

                    return $this->redirect(array('action' => 'login'));
                }
                $this->Flash->error(__('Error while saving data. Please try again'));
            }
        } else {
            return $this->redirect($this->Auth->redirectUrl());
        }
    }

    /**
     * Verifies user account from email
     * @param int $id User ID
     * @param string $token A random generated string that serves as a key
     * @return void
     */
    public function verification($id = null, $token)
    {
        $this->User->id = $id;
        $users = $this->User->find('first', [
            'conditions' => [
                'AND' => [
                    ['User.id' => $id],
                    ['User.token' => $token]
                ]
            ]
        ]);
        if (!$users) {
            throw new NotFoundException();
        }
        if (isset($users['User']['is_verified']) && $users['User']['is_verified'] !== '1' ) {
            $verify = $this->User->find('first',[
                'conditions' => [
                    'User.id' => $id,
                    'User.token' => $token
                    ]
                ]);
            if ($verify) {
                $this->User->saveField('is_verified', 1);
                $this->Flash->success(__('Account successfully verified!'));
            }
        } else {
            $this->Flash->error(__('Account was already verified!'));
        }
    }

    /**
     * User logout
     *
     * @return void
     */
    public function logout()
    {
        $this->Session->delete('redirect');
        return $this->redirect($this->Auth->logout());
    }

    /**
     * User Login
     *
     * @return void
     */
    public function login()
    {
        if (!$this->Auth->user()) {
            $this->Auth->User();
            if ($this->request->is('post')) {
                if ($this->Auth->login()) {
                    return $this->redirect($this->Auth->redirectUrl());
                }
                $this->Flash->error(__('Username or password is incorrect'));
            }
        } else {
            return $this->redirect($this->Auth->redirectUrl());
        }
    }
}
