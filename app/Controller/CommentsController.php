<?php
App::uses('AppController', 'Controller');

/**
 * Comments Controller
 *
 * @property App\Model\Comments
 */
class CommentsController extends AppController
{
    public $helpers = ['Html', 'Form', 'Flash', 'Js'];
    public $uses = ['Post', 'User', 'Comment'];
    public $components = ['Flash', 'RequestHandler', 'Session'];

    /**
     * This function is executed before every action in the controller
     * Allows user to add, edit and delete posts
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
        $this->Auth->allow('add', 'edit', 'delete');
    }

    /**
     * Add user comments based on passed post id
     *
     * @param int $post_id get post id
     * @return void
     */
    public function add() {
        $this->request->onlyAllow('ajax'); // No direct access via browser URL
        $this->autoRender = false;

        // check comment author
        $checkUser = $this->Auth->user('id');
        // show 403 ForbiddenException if other user try to add comments on other profile
        if ($checkUser != $this->request->data['Comment']['userid']) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        if ($this->RequestHandler->isAjax()) {
        //check if user inserted a comment
            if ($this->request->is('post')) {
                $this->Comment->create();
                //set the content of Comment post_id and user_id field
                $data = [
                    'post_id' => $this->request->data['Comment']['postid'],
                    'content' => $this->request->data['Comment']['content'],
                    'user_id' => $this->Auth->user('id')
                ];

                //save request data
                if ($this->Comment->save($data)) {
                    $this->render('index','ajax');
                } else {
                    $this->redirect($this->referer());
                    $this->Flash->error(__('The comment could not be saved. Please, try again.'));
                }
            }
        }
    }

    /**
     * Add user comments based on passed post id
     *
     * @param int $id comment id
     * @return void
     */
    public function edit($id = null) {
        $this->autoRender=false;
        // check if comment exists
        $comments = $this->Comment->find('all', [
            'conditions' => [
                'AND' => [
                    ['Comment.id' => $id],
                    ['Comment.user_id' => $this->Auth->user('id')] // only his/her comment can access
                ]
            ],
        ]);
        if (!$comments) {
            throw new NotFoundException();
        }

        // check comment and comment author
        $checkUser = $this->Comment->find('first', [
            'conditions' => [
                'AND' => [
                    ['Comment.id' => $id],
                    ['Comment.user_id' => $this->Auth->user('id')]
                ]
            ]]);
        // show 403 ForbiddenException if other user try to edit the comment
        if (!$checkUser) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        if ($this->RequestHandler->isAjax()){
            $data = [
                'content' => $this->params['url']['content'],
                'user_id' => $this->Auth->user('id')
            ];
            $this->Comment->id = $this->params['url']['postid'];
            if ($this->Comment->save($data)) {
                $this->render('index','ajax');
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }

            if (!$this->request->data) {
                $this->request->data = $comments;
            }
        }
    }

    /**
     * delete user comments based on passed comment id
     *
     * @param int $id get comment id
     * @return void
     */
    public function delete($id) {
        $this->request->onlyAllow('ajax'); // No direct access via browser URL
        $this->autoRender = false; // No view to render

        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        // check comment and comment author
        $checkUser = $this->Comment->find('first', [
            'conditions' => [
                'AND' => [
                    ['Comment.id' => $id],
                    ['Comment.user_id' => $this->Auth->user('id')]
                ]
            ]]);
        // show 403 ForbiddenException if other user try to edit the comment
        if (!$checkUser) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        $comment = $this->Comment->findById($id);
        // check if comment exists
        if (!$comment) {
            throw new NotFoundException();
        }

        if ($this->RequestHandler->isAjax()) {
            $this->Comment->id = $id;
            if ($this->Comment->delete($this->Comment->id)) {
                $this->render('index','ajax');
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }
    }
}
