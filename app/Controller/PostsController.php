<?php
App::uses('AppController', 'Controller');

class PostsController extends AppController
{
    public $helpers = ['Html', 'Form', 'Flash', 'Js'];
    public $uses = ['Post', 'User', 'Follow', 'Comment', 'Like'];
    public $components = ['Flash', 'Paginator', 'RequestHandler', 'Hashids', 'Session'];
    public $recursive = 2;

     /**
     * This function is executed before every action in the controller
     * Allows user to add, edit and delete posts
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        // Change layout for Ajax requests
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        }
        $this->Auth->allow('add', 'edit', 'delete');
    }

     /**
     * Displays the index view as home page
     * Allows user to save text and image
     *
     * @return void
     */
    public function index()
    {
        // paginate the posts on index or home page
        $this->paginate = [
            'conditions' => [
                'OR' => [ //display own and followed user posts
                    ['followJoin.user_id' => $this->Auth->user('id')],
                    ['User.id' => $this->Auth->user('id')]
                ],
                ['Post.is_deleted' => 0]
            ],
            'joins' => [
                [
                    'table' => 'follows',
                    'alias' => 'followJoin',
                    'type' => 'LEFT',
                    'conditions' => ['User.id = followJoin.following']
                ]
            ],
            'limit' => 4,
            'order' => [
                'Post.created' => 'desc'
            ],
            'group' => ['Post.id']
        ];
        $posts = $this->paginate($this->User->Post);

        // get and pass logged in user credentials to view
        $user = $this->User->find('all', [
            'conditions' => ['User.id' => $this->Auth->user('id')]]);

        // Get list Following
        $optionsFollowing['conditions'] = ['followJoin.user_id' => $this->Auth->user('id')];
        $optionsFollowing['joins'] = [
                ['table' => 'follows',
                'alias' => 'followJoin',
                'type' => 'INNER',
                'conditions' => ['User.id = followJoin.following']
            ]];
        $following = $this->User->find('all', $optionsFollowing);

        // Get list Followers
        $optionsFollowers['conditions'] = ['followJoin.following' => $this->Auth->user('id')];
        $optionsFollowers['joins'] = [
                ['table' => 'follows',
                'alias' => 'followJoin',
                'type' => 'INNER',
                'conditions' => array('User.id = followJoin.user_id')
                ]];
        $followers = $this->User->find('all', $optionsFollowers);

        // create session for redirection after any action made
        $this->Session->write('redirect', $this->request->here());
        //pass the data to view
        $this->set(compact('posts', 'user', 'following', 'followers'));

        //save user posts
        if ($this->request->is('post')) { // check type of request
            // check comment author
            $checkUser = $this->Auth->user('id');
            // show 403 ForbiddenException if other user try to add comments on other profile
            if ($checkUser != $this->request->data['Post']['userid']) {
                throw new UnauthorizedException(); // unauthorized access to the resource
            }

            // check if user inserted a data
            if (!empty($this->request->data['Post']['content']) || !empty($this->request->data['Post']['image']['name'])) {
                $fileName = '';
                $this->Post->create();
                $uploadData = $this->request->data['Post']['image'];
                $filename = basename($uploadData['name']);
                $uploadFolder = WWW_ROOT. 'img';
                $uploadPath =  $uploadFolder . DS . 'posts' . DS .  $filename;

                $this->request->data['Post']['user_id'] = $this->Auth->user('id');
                $this->request->data['Post']['image'] = $filename;
                $this->request->data['Post']['is_deleted'] = 0;

                if ($this->Post->save($this->request->data)) {
                    //save the file to the server after successful save
                    move_uploaded_file($uploadData['tmp_name'], $uploadPath);
                    $this->Flash->success(__('Your post has been saved.'));

                    return $this->redirect(array('action' => 'index'));
                }
                $this->Flash->error(__('Unable to add your post.'));
            } else {
                $this->Flash->error(__('Please insert your tweet first'));
            }
        }
    }

    /**
     * Like post function
     *
     * @param int $id // Handles request post id for like post
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function like($id = null)
    {
        $this->request->onlyAllow('ajax'); // No direct access via browser URL
        $this->autoRender = false; // No view to render
        //show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        // check if user already like the post before
        $like = $this->Like->find('first', [
            'conditions' => [
                'AND' => [
                    ['Like.post_id' => $id],
                    ['Like.user_id' => $this->Auth->user('id')]
                ]
            ]]);

        $post = $this->Post->findById($id);
        //show NotFoundException if post id not exists
        if (!$post) {
            throw new NotFoundException();
        }

        if ($this->RequestHandler->isAjax()) {
            if(empty($like)) { // if first time to like the post, create a new record
                $this->Like->create();
                $data = [
                    'post_id' => $id,
                    'user_id' => $this->Auth->user('id'),
                    'status' => 1
                ];
            } else { // update status to liked
                $data = [
                    'id' => $like['Like']['id'],
                    'post_id' => $id,
                    'user_id' => $this->Auth->user('id'),
                    'status' => 1 // 1 for liked post
                ];
            }
            // save data
            if ($this->Like->save($data)) {
                $this->render('index','ajax');
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('Unable to like post'));
            }
        }

        if(!$this->request->data){
            $this->request->data = $post;
        }
    }

    /**
     * Unlike post function
     *
     * @param int $id // Handles request post id for like post
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function unlike($id = null)
    {
        $this->request->onlyAllow('ajax'); // No direct access via browser URL
        $this->autoRender = false; // No view to render
        //show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        //find the liked post id
        $like = $this->Like->find('first', array('conditions' => array('Like.post_id' => $id)));

        //check if liked post exists
        if (!$like) {
            throw new NotFoundException(__('Post not exists'));
        }

        if ($this->RequestHandler->isAjax()){
            //set data to be saved or edited
            $data = [
                'id' => $like['Like']['id'],
                'post_id' => $id,
                'user_id' => $this->Auth->user('id'),
                'status' => 0 // 0 for unliked post
            ];
            //save the data
            if ($this->Like->save($data)) {
                $this->render('index','ajax');
            } else {
                $this->redirect($this->referer());
                $this->Flash->error(__('Unable to unlike post'));
            }
        }

        if (!$this->request->data){
            $this->request->data = $like;
        }
    }

     /**
     * Retwwet User Post
     *
     * @param int $id // Handles request post id to be retweeted
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function retweet($id = null)
    {
        //decode hash passed id
        $id = $this->Hashids->decode($id);

        //show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        // TEST IF REDUNDANT
        // find the post and check if exists
        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException();
        }

        $posts = $this->Post->find('all', [
            'conditions' => [
                'AND' => [
                    ['Post.id' => $id],
                    ['Post.is_deleted' => 0],
                    ['OR' => [
                            ['Retweet.is_deleted' => 0], // getting retweeted posts
                            ['Retweet.is_deleted' => null] // null or 0
                        ]
                    ]
                ]
            ]]);
        //show NotFoundException if post exist
        if (!$posts) {
            throw new NotFoundException();
        }

        // get and pass logged in user credentials to view
        $user = $this->User->find('all',[
            'conditions' => ['User.id' => $this->Auth->user('id')]]);

        $postId = $post['Post']['post_id']; // get id post id of retweeted post
        $postOrigin = $this->Post->find('first',
            ['conditions' => ['Post.id' => $postId],['Post.author_id' => null]]);// check if retweeting post is already retweeted and get post origin

        $checkPostOrigin = ((!empty($postOrigin)) ? true : false );
        //check if post or put request
        if ($this->request->is(array('post', 'put'))) {
            // check if retweeing post was already retweeted
            if (($checkPostOrigin == false) && (!empty($posts))) {
                //set data to be save from unretweeted posts
                $data = [
                    'user_id' => $this->Auth->user('id'),
                    'post_id' => $id,
                    'content' => $this->request->data['Post']['retweet_content'],
                    'image' => $post['Post']['image'],
                    'author_id' => $post['Post']['user_id'],
                    'retweet_content' => $post['Post']['content'],
                    'is_deleted' => 0
                ];
            } else {
                //set data to be save from retweeted posts
                $data = [
                    'user_id' => $this->Auth->user('id'),
                    'post_id' => $postOrigin['Post']['id'],
                    'content' => $this->request->data['Post']['retweet_content'],
                    'image' => $postOrigin['Post']['image'],
                    'author_id' => $postOrigin['Post']['user_id'],
                    'retweet_content' => $postOrigin['Post']['content'],
                    'is_deleted' => 0
                ];
            }

            // save data
            if ($this->Post->save($data)) {
                $this->Flash->success(__('Your post has been saved.'));

                // redirect to index or home page
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Unable to add post, Please try again'));
        }

        // remain the unmodified data
        if (!$this->request->data) {
            $this->request->data = $posts;
        }

        // pass the data to view
        $this->set(compact('posts', 'user'));
    }

     /**
     * Display single post from passed id
     *
     * @param int $id // Handles request post id to be display
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function view($id = null)
    {
        //decode hash passed id
        $id = $this->Hashids->decode($id);
        //show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        $posts =  $this->Post->find('all', [
            'conditions' => [
                'AND' => [
                    ['Post.id' => $id],
                    ['Post.is_deleted' => 0]
                ]
            ]]);
        // show NotFoundException if post exist
        if (!$posts) {
            throw new NotFoundException();
        }

        // get and pass the current logged in user credentials
        $userAuth = $this->User->find('all', [
            'conditions' => ['User.id' => $this->Auth->user('id')]]);

        $this->set(compact('posts', 'userAuth'));
        $this->Session->write('redirect', $this->here);
    }

     /**
     * Edit single post from passed id
     *
     * @param int $id // Handles request post id to be edited
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function edit($id = null)
    {
        // hash passed id
        $id = $this->Hashids->decode($id);
        // show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        // check post and post author
        $checkUser = $this->Post->find('first', [
            'conditions' => [
                'AND' => [
                    ['Post.id' => $id],
                    ['Post.user_id' => $this->Auth->user('id')]
                ]
            ]]);
        // show 403 ForbiddenException if other user try to edit the post
        if (!$checkUser) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        $posts = $this->Post->find('all', [
            'conditions' => [
                'AND' => [
                    ['Post.id' => $id],
                    ['Post.is_deleted' => '0']
                ]
            ]]);
        // show NotFoundException if post not exist
        if (!$posts) {
            throw new NotFoundException();
        }

        // get and pass the current logged in user credentials
        $userAuth = $this->User->find('all', [
            'conditions' => ['User.id' => $this->Auth->user('id')]]);

        $this->set(compact('posts', 'userAuth'));

        if ($this->request->is(['post', 'put'])) {
            $filename = '';
            $this->request->data['Post']['user_id'] = $this->Auth->user('id');
            $this->request->data['Post']['is_deleted'] = 0;
            if (!empty($this->request->data['Post']['image']['name'])) {

                $uploadData = $this->request->data['Post']['image'];
                $filename = basename($uploadData['name']);
                $uploadFolder = WWW_ROOT. 'img' ;
                $filename = time() .'_'. $filename;
                $uploadPath =  $uploadFolder . DS . 'posts' . DS .  $filename;

                if (!move_uploaded_file($uploadData['tmp_name'], $uploadPath)) {
                    $this->Flash->error(__('Error Uploading Image, Please try again'));
                }

                $this->request->data['Post']['image'] = $filename;
            } else {
                unset( $this->request->data['Post']['image'] );
            }

            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Successfully updated post'));

                $this->redirect($this->Session->read('redirect'));
                $this->Session->delete('redirect');
            }
            $this->Flash->error(__('Error updating post, Please try again'));
        }

        if (!$this->request->data) {
            $this->request->data = $posts;
        }
    }

    /**
     * Delete single post from passed id
     * Used Manual soft delete
     *
     * @param int $id // Handles request post id to be edited
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     * @throws \Cake\Network\Exception\MethodNotAllowedException When the request is not post or put
     */
    public function delete($id = null)
    {
        // hash passed id
        $id = $this->Hashids->decode($id);
        // show NotFoundException if id is null
        if (!$id) {
            throw new NotFoundException();
        }

        // check request method
        if ($this->request->is('get')) { //should not be get method to prevent function from web crawlers
            throw new MethodNotAllowedException();
        }

        // check post and post author
        $checkUser = $this->Post->find('first', [
            'conditions' => [
                'AND' => [
                    ['Post.id' => $id],
                    ['Post.user_id' => $this->Auth->user('id')]
                ]
            ]]);
        // show 403 ForbiddenException if other user try to edit the post
        if (!$checkUser) {
            throw new ForbiddenException(); // unauthorized access to the resource
        }

        // check post if exists
        $post = $this->Post->find('first', [
            'conditions' => [
                'AND' => [
                    'Post.id' => $id,
                    'Post.is_deleted' => 0 // only delete undeleted posts
                ]
            ]]);
        // show NotFoundException if post not exist and if other user try to delete the post
        if (!$post) {
            throw new NotFoundException();
        }

        // set data for deleted post
        $this->request->data['Post']['is_deleted'] = 1; // field default is 0, 1 means deleted post
        $this->request->data['Post']['date_deleted'] = date("Y-m-d H:i:s");

        // check if request is post
        if ($this->request->is('post')) {
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Post successfully deleted.'));

                // redirect to index if the referrer is on post view
                if (strpos($this->Session->read('redirect'), 'posts/view') !== false) {
                    //redirect to index or home page
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->redirect($this->Session->read('redirect'));
                    $this->Session->delete('redirect');
                }
            }
            $this->Flash->error(__('Unable to delete your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }
}
