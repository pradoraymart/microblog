<?= $this->start('navbar');?>
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #e44d3a !important;">
    <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <?= $this->Html->link(
                    'Home <span class="sr-only">(current)</span>',
                    ['action' => 'index'],
                    ['escape' => false,
                    'title' => __('home'),
                    'class' => 'nav-link']
                ) ?>
            </li>
            <li class="nav-item">
                <?= $this->Html->link(
                    'Profile',
                    ['action' => 'view',
                    'controller' => 'users',
                    AuthComponent::user('id')],
                    ['escape' => false,
                    'title' => __('Profile'),
                    'class' => 'nav-link']
                ) ?>
            </li>
        </ul>
    </div>
    <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
    <?= $this->Html->Image($users['User']['image']) ?>
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php foreach ($user as $users): ?>

            <?php if (AuthComponent::user('id')): ?>
             <?= $users['User']['first_name']. " " .$users['User']['last_name']?>
            <?php endif; ?>
            <?php endforeach; ?>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <?= $this->Html->link('Edit Profile',
                    array(
                        'controller' => 'users',
                        'action' => 'edit',
                        AuthComponent::user('id')
                    ),
                    array(
                        'class' => 'dropdown-item'
                    )
                    );?>
                <?= $this->Html->link('Logout',
                    array(
                        'controller' => 'users',
                        'action' => 'logout'
                    ),
                    array(
                        'class' => 'dropdown-item'
                    )
                    );?>
            </div>
        </li>
    </ul>
</nav>
<?= $this->end();?>
