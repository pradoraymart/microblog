<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

header('content-type: application/json');
header('Access-Control-Allow-Origin: *');
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("X-XSS-Protection: 1; mode=block");
header("Expires: 0");

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo $this->Html->charset(); ?>
  <title>
    <?php echo 'Microblog' ?>:
    <?php echo $this->fetch('title'); ?>
  </title>
  <?php
    echo $this->Html->meta('icon');

    echo $this->Html->css(['style', 'bootstrap.min', 'font-awesome.min']);

    echo $this->fetch('meta');
    echo $this->fetch('css');

    echo $this->Html->script(['jquery', 'jquery-3.4.1.min', 'bootstrap.min']);
    echo $this->fetch('script');
  ?>
</head>
<body>
  <?php if ($authUser): ?>
  <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #e44d3a !important;">
    <a class="navbar-brand" href="#"> Microblog </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <?php
          $controller = $this->request->params['controller'];
          $action = $this->request->params['action'];
        ?>
        <li class="nav-item <?php echo (($controller == 'posts') && ($action == 'index')) ? 'active' :'inactive' ?>">
          <?php
          $userId = HashidsComponent::encode(AuthComponent::user('id'));
          echo $this->Html->link(
            '<i class="fa fa-home"></i> Home <span class="sr-only">(current)</span>',
              ['controller' => 'posts',
              'action' => 'index'],
              ['escape' => false,
              'title' => __('home'),
              'class' => 'nav-link']);?>
        </li>
        <!-- / navbar-item -->
        <li class="nav-item <?php echo (($controller == 'users') && ($action == 'view')) ? 'active' :'inactive' ?>">
          <?= $this->Html->link(
            '<i class="fa fa-user"></i> Profile',
            ['action' => 'view',
            'controller' => 'users',
            $userId],
            ['escape' => false,
            'title' => __('Profile'),
            'class' => 'nav-link']
          ) ?>
        </li>
        <!-- / navbar-item -->
      </ul>
      <!-- / navbar-nav -->
    </div>
    <!-- / #navbarNavDropdown -->
    <div class="search-form">
      <?= $this->Form->create('User',
          ['type' => 'get',
          'url' => ['controller' => 'users', 'action' => 'search'],
          'class' => 'form-inline',
          'autocomplete' => 'off']);?>
      <?= $this->Form->input('search',
          ['class' => 'form-control mr-sm-2',
          'placeholder' => 'Search',
          'label' => false]);?>
      <?= $this->Form->button('Search',
          ['action' => 'search',
          'controller' => 'users',
          'class' => 'btn btn-outline-success my-2 my-sm-0']);?>
      <?= $this->Form->end(); ?>
    </div>
    <!--/ search-form  -->
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php if (AuthComponent::user('id')): ?>
          <?= h(AuthComponent::user('first_name'). " " . AuthComponent::user('last_name'))?>
        <?php endif; ?>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <?php $accountId = HashidsComponent::encode(AuthComponent::user('id'));
            echo $this->Html->link('Edit Profile',
              ['controller' => 'users', 'action' => 'edit'],
              ['class' => 'dropdown-item']);
          ?>
          <?= $this->Html->link('Logout',
              ['controller' => 'users', 'action' => 'logout'],
              ['class' => 'dropdown-item']);?>
        </div>
        <!--/ dropdown-menu-->
      </li>
      <!--/ nav-item dropdown -->
    </ul>
    <!--/ navbar-nav -->
  </nav>
  <!--/ navbar navbar-expand-lg navbar-light bg-light-->
  <?php endif; ?>
    <div id="container">
      <div id="header">
      </div>
      <div id="content">
        <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>
      </div>
      <div id="footer">
        <!-- Footer -->
        <footer class="page-footer font-small blue pt-4">
        <!-- Copyright -->
          <div class="footer-copyright text-center py-3">© 2019 Copyright:
          <a href="http://dev1.ynsdev.pw">Microblog</a>
          </div>
        <!-- Copyright -->
        </footer>
        <!-- Footer -->
      </div>
      <!-- /Footer -->

    <script type="text/javascript">

  $(function() {

    //call function that counts the character tweets
    retweetCounter();

    // Show and hide comments
    $(document).on("click", ".commentShow", function(e) {
      var id = $(this).attr("id");
      e.preventDefault();
      var target = jQuery(".comment-sec" + id);
      if (target.is(':visible')) {
        $('.comment-sec' + id).css('cssText', 'display: none !important');
      } else {
        $('.comment-sec' + id).css('cssText', 'display: block !important');
      }
    });

    // delete comments
    $(document).on('click','.delButton', function(e){
    //prevent page from loading
      e.preventDefault();
      var element = $(this);
      var del_id = element.attr("id");

      if (confirm("Are you sure you want to delete this Comment?")){
        $.ajax({
          type: "POST",
          url: "../../comments/delete/" + del_id,
          success: function(data,textStatus,xhr){
            window.location.reload();
          },
          error: function(xhr,textStatus,error){
            window.location.reload();
          }
        });
      }
      return false;
    });

  //Edit Comments
  $("#editComment").submit(function(e){
    //prevent page from loading
    e.preventDefault();
    //disable send button after submit
    var formData = $('form.editComment').serialize();
    var commentPostId = $('#commentId').val();

    $("#submitEditedComment").attr("disabled", true);

    $.ajax({
      type: 'GET',
      url: "../../comments/edit/" + commentPostId,
      data: formData,
      success: function(data,textStatus,xhr){
        window.location.reload();
      },
      error: function(xhr,textStatus,error){
        // show the response
        window.location.reload();
      }
    });
    return false;
  });

  $(document).on('click','.editButton',function(e){
      //prevent page from loading
      e.preventDefault();
      var element = $(this);
      var commentId = element.attr("id");
      var commentContent = element.attr("data-id");
      $("#commentId").val(commentId);
      $("#commentContent").val(commentContent);
      $("#submitComment").prop('id', 'submitEditedComment');
      $('#editComments').modal('show');
  });
});

  function likePost(id) {
    var totalLikes = parseInt($('.countLikes' + id).text());
    var addLike = totalLikes + 1;
    $.ajax({
      type: 'GET',
      url: "../../posts/like/" + id,
      type: 'ajax',
      success: function(data) {
        $('.commentLabel' + id).css('cssText', 'color: #e44d3a !important');
        $('.commentLabel' + id).attr("onclick", "unlikePost("+ id +")");
        $('.countLikes' + id).text(addLike);
      },
      error: function(data){
      }
    });
  }

  function unlikePost(id) {
    var totalLikes = parseInt($('.countLikes' + id).text());
    var subLike = totalLikes - 1;
    $.ajax({
      type: 'GET',
      url: "../../posts/unlike/" + id,
      type: 'ajax',
      success: function(data) {
        $('.commentLabel' + id).css('cssText', 'color: #b2b2b2 !important');
        $('.commentLabel' + id).attr("onclick", "likePost("+ id +")");
        $('.countLikes' + id).text(subLike);
      },
      error: function(data){
      }
    });
  }

  function retweetCounter(){
    var maxLength = 140;
    $('#PostContent').keyup(function() {
      var textlen = maxLength - $(this).val().length;
      $('#remainingCharacter').text(textlen);
    });
    $('#PostRetweetContent').keyup(function() {
      var textlen = maxLength - $(this).val().length;
      $('#remainingCharacter').text(textlen);
    });
    $('#commentContent').keyup(function() {
      var textlen = maxLength - $(this).val().length;
      $('#commentRemainingCharacter').text(textlen);
    });
    $('#UserSelfDescription').keyup(function() {
      var textlen = maxLength - $(this).val().length;
      $('#remainingCharacter').text(textlen);
    });
  }
    </script>
  </body>
</html>
