<div class="main-section-data">
  <div class="row">
    <div class="col-lg-6 col-md-8 offset-md-3 no-pd">
      <div class="main-ws-sec">
        <div class="posts-section">
        <?php foreach ($posts as $post): ?>
          <div class="post-bar">
          <?php foreach ($userAuth as $userAuth): ?>
            <div class="post_topbar">
              <div class="usy-dt">
              <?= $this->Html->image("profile/" . h($userAuth['User']['image']));?>
                <div class="usy-name">
                  <h3>
                    <?php
                      $userId = HashidsComponent::encode(h($userAuth['User']['id']));
                      echo $this->Html->link(h($userAuth['User']['first_name']). " " . h($userAuth['User']['last_name']),
                        ['controller' => 'users',
                        'action' => 'view',
                        $userId]); ?>
                  </h3>
                  <span> <?= h($userAuth['User']['username'])?> </span>
                </div>
                <!--/ usy-name -->
              </div>
              <!--/ usy-dt -->
            </div>
            <!--/ post_topbar -->
            <?php endforeach; ?>
            <div class="post_content">
              <?php if (isset($post['Post']['post_id'])) {?>
                <?= $this->Form->create('Post', ['enctype' => 'multipart/form-data']);?>
                <div class="form-group">
                  <?= $this->Form->textarea('content',
                    ['rows' => '2',
                    'cols' => '100',
                    'maxlength' => 140,
                    'class' => 'form-control',
                    'value' => $post['Post']['content'],
                    'placeholder' => 'Add caption or comment...']);?>
                  <small class="form-text text-muted"  style="float:left;">
                    <span id="remainingCharacter">140</span>
                      Character(s) Remaining
                  </small><br>
                </div>
                <!--/ form-group -->
                <div class="retweet-bar col-md-11 offset-md-1">
                  <div class="retweet_bar">
                    <div class="post_topbar">
                      <div class="usy-dt">
                        <?= $this->Html->image("profile/" . h($post['AuthorRetweet']['image']));?>
                        <div class="usy-name">
                          <h6>
                            <?php
                            $authorId = HashidsComponent::encode($post['AuthorRetweet']['id']);
                            echo $this->Html->link(h($post['AuthorRetweet']['first_name']). " " . h($post['AuthorRetweet']['last_name']),
                              ['controller' => 'users',
                              'action' => 'view',
                              $authorId]);?>
                          </h6>
                          <span>
                            <i class="fa fa-clock-o"></i>
                            <?= $this->Time->niceShort(h($post['Retweet']['created']))?>
                          </span>
                        </div>
                        <!-- / usy-name -->
                      </div>
                      <!-- / usy-dt -->
                    </div>
                    <!-- / post_topbar -->
                    <?php $postId = HashidsComponent::encode(h($post['Post']['post_id']));?>
                    <div class = "post_content" onclick="window.location.href='<?= Router::url(array('controller' => 'posts', 'action' => 'view', $postId))?>'">
                      <?php if (empty($post['Post']['image'])) {
                          echo '<p>';
                            echo h($post['Post']['retweet_content']);
                          echo '</p>';
                        } else {
                          echo '<p>';
                            echo h($post['Post']['retweet_content']);
                          echo '</p>';
                          echo $this->Html->image("posts/" . h($post['Post']['image']));
                        }?>
                    </div>
                    <!-- / post_content -->
                  </div>
                  <!-- / retweet-bar-->
                </div>
                <!-- / retweet-bar col-md-11 -->
              <div class="form-group">
                <?php
                  echo $this->Form->button('Save',
                    ['action' => 'edit',
                    'class'=>'btn btn-primary float-right',
                    'onclick' => "this.form.submit(); this.disabled=true;"]);
                  echo $this->Form->end();
                ?>
              </div>
              <?php } else { ?>
                <?= $this->Form->create('Post', ['enctype' => 'multipart/form-data']);?>
                <div class="form-group">
                  <label for="content">
                  <?= $this->Form->textarea('content',
                  ['rows' => '2',
                    'cols' => '100',
                    'maxlength' => 140,
                    'class' => 'form-control',
                    'value' => $post['Post']['content'],
                    'placeholder' => 'Add caption or comment...']); ?>
                  <small class="form-text text-muted"  style="float:left;">
                    <span id="remainingCharacter">140</span>
                      Character(s) Remaining
                  </small><br>
                </div>
                <div class="form-row">
                  <?php if (isset($post['Post']['image'])) { ?>
                    <div class="col form-group">
                      <?= $this->Form->input('image',
                        ['type' => 'file',
                        'title' => $post['Post']['image'],
                        'value' => $post['Post']['image'],
                        'label' => false])?>
                    </div>
                    <!--/ col form-group -->
                  <?php } ?>
                  <div class="col form-group">
                    <?php
                      echo $this->Form->button('Save',
                        ['action' => 'edit',
                        'class'=>'btn btn-primary float-right',
                        'onclick' => "this.form.submit(); this.disabled=true;"]);
                      echo $this->Form->end();
                    ?>
                  </div>
                  <!--/ col form-group -->
                </div>
                <!--/ form-row -->
              <?php } ?>
            </div>
            <!--/ post_content -->
          </div>
          <!--/ post-bar -->
          <?php endforeach; ?>
          <!--/ foreach -->
        </div>
        <!--/ main-section -->
      </div>
      <!--/ main-ws-sec -->
    </div>
    <!--/ col-lg-6 col-md-8 no-pd -->
  </div>
  <!--/ row -->
</div>
<!--/ main-section-data -->
