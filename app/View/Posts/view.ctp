<div class="main-section-data">
  <div class="row">
    <div class="col-lg-6 col-md-8 offset-md-3 no-pd">
      <div id="posts-list" class="main-ws-sec">
        <?php foreach ($posts as $post): ?>
        <div class="posts-section">
          <div class="post-bar">
            <div class="post_topbar">
              <div class="usy-dt">
                <?= $this->Html->image("profile/" . h($post['User']['image'])); ?>
                <div class="usy-name">
                  <h3>
                    <?php
                      $userId = HashidsComponent::encode($post['Post']['user_id']);
                      echo $this->Html->link(h($post['User']['first_name']). " " . h($post['User']['last_name']),
                        ['controller' => 'users',
                        'action' => 'view',
                        $userId]);?>
                  </h3>
                  <span>
                    <i class="fa fa-clock-o"></i>
                    <?= $this->Time->niceShort(h($post['Post']['created']))?>
                  </span>
                </div>
                <!--/ usy-name -->
              </div>
              <!--/ usy-dt -->
              <div class="ed-opts">
                <?php
                  $id = HashidsComponent::encode($post['Post']['id']);
                  if (AuthComponent::user('id') == $post['Post']['user_id']) {?>
                    <?= $this->Html->link(
                      '<i class="fa fa-edit"></i>',
                      ['action' => 'edit', $id],
                      ['escape' => false, 'title' => __('Edit')]
                    )?>
                    <?= $this->Form->postLink(
                    $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['controller' => 'posts', 'action' => 'delete', $id],
                        ['escape' => false],
                      __('Are you sure you want to delete?', $id)
                    );?>
                  <?php } ?>
              </div>
              <!--/ ed-opts -->
            </div>
            <!--/ post_topbar -->
            <?php if (isset($post['Post']['post_id'])) { ?>
              <div class="post_content">
                <p> <?= h($post['Post']['content']) ?> </p>
                <div class="retweet-bar col-md-11 offset-md-1">
                  <div class="retweet_bar">
                    <div class="post_topbar">
                      <div class="usy-dt">
                        <?= $this->Html->image("profile/" . h($post['AuthorRetweet']['image']));?>
                        <div class="usy-name">
                          <h6>
                            <?php
                              $AuthorId = HashidsComponent::encode($post['AuthorRetweet']['id']);
                              echo $this->Html->link(h($post['AuthorRetweet']['first_name']). " " . h($post['AuthorRetweet']['last_name']),
                                ['controller' => 'users',
                                'action' => 'view',
                                $AuthorId]);?>
                          </h6>
                          <span>
                            <i class="fa fa-clock-o"></i>
                            <?= $this->Time->niceShort(h($post['Retweet']['created'])) ?>
                          </span>
                        </div>
                      </div>
                    </div>
                    <?php $retweetPostId = HashidsComponent::encode($post['Post']['post_id']); ?>
                    <div class="post_content" onclick="window.location.href='<?= Router::url(['controller' => 'posts', 'action' => 'view', $retweetPostId])?>'">
                      <?php
                        // check if post has text or image
                        if (empty($post['Post']['image'])) {
                          // check if the origin of post has been deleted
                          if ($post['Retweet']['is_deleted'] == 1) {
                            echo '<p style="color:#e44d3a;"> Content is not available</p>';
                          } else {
                            echo '<p>';
                              echo h($post['Post']['retweet_content']);
                            echo '</p>';
                          }
                        } else {
                          if ($post['Retweet']['is_deleted'] == 1) {
                            echo '<p style="color:#e44d3a;"> Content Not Available</p>';
                          } else {
                            echo '<p>';
                              echo h($post['Post']['retweet_content']);
                            echo '</p>';
                            echo $this->Html->image("posts/" . h($post['Post']['image']));
                          }
                        }?>
                    </div>
                    <!--/ post_content -->
                  </div>
                  <!--/ retweet_bar -->
                </div><!-- end of retweet-bar col-md-11 -->
              </div><!-- end of post content -->
            <?php } else { ?>
              <div class="post_content" onclick="window.location.href='<?= Router::url(['controller' => 'posts', 'action' => 'view', $id])?>'">
                <?php
                  if (empty($post['Post']['image'])) {
                    echo '<p>';
                      echo h($post['Post']['content']);
                    echo '</p>';
                  } else {
                    echo '<p>';
                      echo h($post['Post']['content']);
                    echo '</p>';
                    echo $this->Html->image("posts/" . h($post['Post']['image']));
                  }?>
              </div>
              <!--/ post_content -->
            <?php } ?>
            <div class="job-status-bar">
              <ul class="like-com">
                <?php $likeId = HashidsComponent::decode($id); ?>
                <li style="margin-right: 0px !important;">
                  <p class="countLikes<?=$likeId?>"><?= sizeof($post['Like']) ?></p>
                </li>
                <?php
                  $isLiked = false;
                  for ($i = 0; $i < sizeof($post['Like']); $i++) {
                    $isLiked = ($post['Like'][$i]['user_id'] == AuthComponent::user('id') ? true : false);
                  } ?>
                  <?php if ($isLiked) { ?>
                    <li class="btnLike">
                      <p style="color:#e44d3a !important" class="com comLabel commentLabel<?=$likeId?>" onclick="unlikePost(<?=$likeId?>);">
                        <i class="fa fa-thumbs-up"></i> Like
                      </p>
                    </li>
                  <?php } else { ?>
                    <li class="btnLike">
                      <p class="com comLabel commentLabel<?=$likeId?>" onclick="likePost(<?=$likeId?>);">
                        <i class="fa fa-thumbs-up"></i> Like
                      </p>
                    </li>
                  <?php } ?>
                <li>
                  <a title="" class="com commentShow" id="<?=$id?>" ><i class="fa fa-comment"></i> <?= sizeof($post['Comment'])?> Comment </a>
                </li>
                <li>
                  <?= $this->Html->link(
                    '<i class="fa fa-retweet"></i> ' .sizeof($post['NoOfRetweets']). ' Retweet',
                      ['controller' => 'posts',
                      'action' => 'retweet',
                      $id],
                      ['escape' => false,
                      'class' => 'com']);?>
                </li>
              </ul>
            </div>
            <!--/ job-status-bar -->
          </div>
          <!--/ post-bar-->
          <div class="comment-section" style="padding-left:15px !important;">
            <?php if (sizeof($post['Comment']) > 0) { ?>
              <?php for($i = 0; $i < sizeof($post['Comment']); $i++) { ?>
                <div class="comment-sec<?=$id?>">
                  <ul>
                    <li>
                      <div id="commentSection" class="comment-list">
                        <div class="bg-img">
                          <?= $this->Html->image("profile/" . h($post['Comment'][$i]['User']['image']));?>
                        </div>
                        <div class="comment">
                          <h3>
                            <?php
                            $commentUserId = HashidsComponent::encode($post['Comment'][$i]['User']['id']);
                            echo $this->Html->link(h($post['Comment'][$i]['User']['first_name']) . " " . h($post['Comment'][$i]['User']['last_name']),
                              ['controller' => 'users',
                              'action' => 'view',
                              $commentUserId]);
                            if (AuthComponent::user('id') == $post['Comment'][$i]['user_id']) {
                              $commentId = $post['Comment'][$i]['id']; ?>
                              <i id="<?= $commentId ?>" data-id="<?= h($post['Comment'][$i]['content']);?>" class="editButton editBtn fa fa-edit"></i>
                              <?= $this->Html->link('<i class="fa fa-trash"></i>', [],
                                  ['escape' => false,
                                  'id' => $commentId,
                                  'class' => 'delButton delBtn']);
                              }
                            ?>
                          </h3>
                          <span><i class="fa fa-clock-o"></i> <?= $this->Time->niceShort(h($post['Comment'][$i]['created']))?></span>
                          <div class="comment-list-content">
                            <p><?= h($post['Comment'][$i]['content'])?> </p>
                          </div>
                        </div>
                        <!--/ comment -->
                      </div>
                      <!--/ comment-list -->
                    </li>
                  </ul>
                </div>
                <!--/ comment-sec -->
              <?php } ?>
            <?php } ?>
            <div class="row post-comment" style="padding-bottom:30px;">
              <?php foreach ($userAuth as $userAuth): ?>
                <div class="bg-img">
                  <?= $this->Html->image("profile/" . h($userAuth['User']['image']));?>
                </div>
              <?php endforeach; ?>
              <div class="comment_box">
                <?= $this->Form->create('Comment',
                    ['id' => 'saveComment',
                    'class' => 'saveComment'])?>
                <?php
                  $commentId = HashidsComponent::decode($id);
                  echo $this->Form->input('postid',
                    ['type' => 'hidden',
                    'value' => $commentId,
                    'label' => false])
                ?>
                <?= $this->Form->input('userid',
                  ['type' => 'hidden',
                  'value' => AuthComponent::user('id'),
                  'label' => false])?>
                <?= $this->Form->input('content',
                    ['placeholder' => 'Post a comment',
                    'maxlength' => 140,
                    'label' => false])?>
              </div>
              <div>
              <?= $this->Form->input('Send',
                    ['type' => 'button',
                    'label' => false,
                    'id' => 'submitComment',
                    'class'=>'comment_box2']);?>
              <?= $this->Form->end() ?>
              </div>
            </div>
            <!--/ post-comment-->
          </div>
          <!--/ comment-section-->
          <?php endforeach; ?>
        </div>
      </div>
      <!--/ main-ws-sec -->
    </div>
    <!--/ col-lg-6 col-md-8 no-pd -->
  </div>
  <!--/ row -->
</div>
<!--/ main-section-data -->

<!-- Edit Comment Modal -->
<!-- Modal -->
<div class="modal fade" id="editComments" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title">Edit Comment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </div>
      <!--/ modal-header -->
      <div class="modal-body">
        <?= $this->Form->create('Comment',
            ['type' => 'get',
            'id' => 'editComment',
            'class' => 'editComment']);?>
        <?= $this->Form->input('postid',
          ['id' => 'commentId',
          'type' => 'hidden',
          'label' => false]);?>
        <div class="form-group">
          <?= $this->Form->input('content',
            ['type' => 'textarea',
            'rows' => '3',
            'cols' => '60',
            'id' => 'commentContent',
            'class' => 'form-control',
            'maxlength' => 140,
            'label' => false]);?>
            <small class="form-text text-muted">
              <span style="float:left;" id="commentRemainingCharacter">140</span>
            </small>
        </div>
        <div class="form-group">
          <?= $this->Form->input('Save Changes',
            ['type' => 'button',
            'label' => false,
            'id' => 'submitEditedComment',
            'class'=>'comment_box2']);?>
          <?= $this->Form->end() ?>
        </div>
        <!--/ form-group -->
      </div>
      <!--/ modal-body -->
    </div>
    <!--/ modal-content -->
  </div>
  <!--/ modal-dialog -->
</div>
<!--/ modal fade -->

<script type="text/javascript">
$(document).ready(function () {
  $(".saveComment").submit(function(e){
    //prevent page from loading
    e.preventDefault();
    //disable send button after submit
    $("#submitComment").attr("disabled", true);

    var formData = $(this).serializeArray();
    var id = $('#CommentPostid').val();
    $.ajax({
      type: 'POST',
      url: "../../comments/add/",
      data: formData,
      success: function(data,textStatus,xhr){
        window.location.reload();
      },
      error: function(xhr,textStatus,error){
        window.location.reload();
      }
    });
    return false;
  });
});
</script>
