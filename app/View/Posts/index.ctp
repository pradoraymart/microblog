<div class="main-section-data">
  <div class="row">
    <div class="col-lg-3 col-md-4 pd-left-none no-pd">
      <div class="main-left-sidebar no-margin">
        <div class="user-data full-width">
        <?php foreach ($user as $users): ?>
          <div class="user-profile">
            <div class="username-dt">
              <div data-toggle="modal" data-target="#viewImage" class="usr-pic">
                <?= $this->Html->image("profile/" . h($users['User']['image']));?>
              </div>
              <!--/ usr-pic-->
            </div>
            <!--/ username-dt end-->
            <div class="user-specs">
              <h3>
                <?php
                  $userId = HashidsComponent::encode(AuthComponent::user('id'));
                  echo $this->Html->link(h($users['User']['first_name']). " " . h($users['User']['last_name']),
                    ['controller' => 'users',
                    'action' => 'view',
                    $userId]);
                ?>
              </h3>
              <span><?= h($users['User']['username'])?></span><br>
              <?php if (!empty($users['User']['self_description'])) { ?>
                <span><?= h($users['User']['self_description'])?></span>
              <?php } ?>
              <?php endforeach; ?>
            </div>
            <!--/ user-profile -->
          </div>
          <!--/ user-profile end-->
          <ul class="user-fw-status">
            <li>
              <h4 data-toggle="modal" data-target="#viewFollowing">Following</h4>
              <span><?= h(sizeof($following))?></span>
            </li>
            <li>
              <h4 data-toggle="modal" data-target="#viewFollowers">Followers</h4>
              <span><?= h(sizeof($followers))?></span>
            </li>
            <li>
              <?= $this->Html->link('View Profile',
                ['controller' => 'users',
                'action' => 'view',
                $userId]) ?>
            </li>
          </ul>
          <!--/ user-fw-status-->
        </div>
        <!--/ user-data -->
      </div>
      <!--/ main-left-sidebar -->
    </div>
    <!--/ col-lg-3 col-md-4 pd-left-none no-pd-->

    <div class="col-lg-6 col-md-8 no-pd">
      <div id="posts-list" class="main-ws-sec">
        <div class="posts-section">
          <div class="post_topbar">
            <?= $this->Form->create('Post', ['enctype' => 'multipart/form-data']);?>
            <div class="form-group">
              <?= $this->Form->textarea('content',
                ['rows' => '3',
                'cols' => '100',
                'class' => 'form-control',
                'maxlength' => 140,
                'placeholder' => 'What\'s Happening?']); ?>
              <small class="form-text text-muted" style="float:left;" >
                <span id="remainingCharacter">140</span>
                  Character(s) Remaining
              </small><br>
            </div>
            <!--/ form-group -->
            <div class="form-row">
              <div class="col form-group">
                <?= $this->Form->input('userid',
                  ['type' => 'hidden',
                  'value' => AuthComponent::user('id'),
                  'label' => false])?>
                <?= $this->Form->input('image',
                  ['type' => 'file',
                  'label' => false]) ?>
              </div>
              <!--/ col form-group -->
              <div class="col form-group">
                <?php
                  echo $this->Form->button('Tweet',
                    ['action' => 'add',
                    'class'=>'btn btn-primary float-right',
                    'onclick' => "this.form.submit(); this.disabled=true;"]);
                  echo $this->Form->end();
                ?>
              </div>
              <!--/ col form-group -->
            </div>
            <!--/ form-row -->
          </div> <!-- end of post-topbar -->

          <?php foreach ($posts as $post): ?>
          <div class="post-bar">
            <div class="post_topbar">
              <div class="usy-dt">
                <?= $this->Html->image("profile/" . h($post['User']['image']));?>
                <div class="usy-name">
                  <h3>
                    <?php
                      //hash post author id
                      $postAuthorId = HashidsComponent::encode($post['User']['id']);
                      echo $this->Html->link(h($post['User']['first_name']). " " . h($post['User']['last_name']),
                      ['controller' => 'users',
                      'action' => 'view',
                      $postAuthorId]) ?>
                  </h3>
                  <span>
                    <i class="fa fa-clock-o"></i>
                    <?= $this->Time->niceShort(h($post['Post']['created']))?>
                  </span>
                </div>
              </div>
              <div class="ed-opts">
                <?php
                  $id = HashidsComponent::encode($post['Post']['id']);
                  if (AuthComponent::user('id') == $post['Post']['user_id']) {?>
                    <?= $this->Html->link(
                      '<i class="fa fa-edit"></i>',
                      ['action' => 'edit', $id],
                      ['escape' => false, 'title' => __('Edit')]) ?>
                    <?= $this->Form->postLink(
                      $this->Html->tag('i', '', array('class' => 'fa fa-trash')),
                        ['controller'=>'posts', 'action' => 'delete', $id],
                        ['escape' => false],
                        __('Are you sure you want to delete?', $id)) ?>
                <?php } ?>
              </div>
              <!--/ ed-opts -->
            </div>
            <!--/ post_topbar -->

            <?php
            //IF RETWEETED POST
            if (isset($post['Post']['post_id'])) {
              $postID = $post['Post']['id'];?>
              <div class="post_content" onclick="window.location.href='<?= Router::url(array('controller'=>'posts', 'action'=>'view', $id))?>'">
                <p><?= h($post['Post']['content']) ?></p>
                <div class="retweet-bar col-md-11 offset-md-1">
                  <div class="retweet_bar">
                    <div class="post_topbar">
                      <div class="usy-dt">
                        <?= $this->Html->image("profile/" . h($post['AuthorRetweet']['image']));?>
                        <div class="usy-name">
                          <h6>
                          <?php
                            $authorId = HashidsComponent::encode($post['AuthorRetweet']['id']);
                            echo $this->Html->link(h($post['AuthorRetweet']['first_name']). " " . h($post['AuthorRetweet']['last_name']),
                            ['controller' => 'users',
                            'action' => 'view',
                            $authorId]) ?>
                          </h6>
                          <span>
                            <i class="fa fa-clock-o"></i>
                            <?= $this->Time->niceShort(h($post['Retweet']['created'])) ?>
                          </span>
                        </div>
                        <!--/ usy-name -->
                      </div>
                      <!--/ usy-dt -->
                    </div>
                    <!--/ post_topbar -->
                    <div class="post_content" onclick="window.location.href='<?= Router::url(array('controller' => 'posts', 'action' => 'view', $id))?>'">
                      <?php if (empty($post['Post']['image'])) {
                        if ($post['Retweet']['is_deleted'] == 1) {
                          echo '<p style="color:#e44d3a;"> Content is not available</p>';
                        } else {
                          echo '<p>';
                            echo h($post['Post']['retweet_content']);
                          echo '</p>';
                        }
                      } else {
                        if ($post['Retweet']['is_deleted'] == 1) {
                          echo '<p style="color:#e44d3a;"> Content Not Available</p>';
                        } else {
                          echo '<p>';
                            echo h($post['Post']['retweet_content']);
                          echo '</p>';
                          echo $this->Html->image("posts/" . h($post['Post']['image']));
                        }
                      }?>
                    </div>
                    <!--/ post_content -->
                  </div>
                  <!--/ retweet_bar -->
                </div>
                <!--/ retweet-bar col-md-11 -->
              </div>
              <!--/ post content -->
            <?php } else { ?>
              <div class="post_content" onclick="window.location.href='<?= Router::url(['controller' => 'posts', 'action' => 'view', $id])?>'">
                <?php
                  $postID = $post['Post']['id'];
                  if (empty($post['Post']['image'])) {
                    echo '<p>';
                      echo h($post['Post']['content']);
                    echo '</p>';
                  } else {
                    echo '<p>';
                      echo h($post['Post']['content']);
                    echo '</p>';
                    echo $this->Html->image("posts/" . h($post['Post']['image']));
                  }
                ?>
              </div>
            <?php } ?>
            <div id="statusSection" class="job-status-bar">
              <ul class="like-com">
                <?php $likeId = HashidsComponent::decode($id); ?>
              <li style="margin-right: 0px !important;">
                <p class="countLikes<?=$likeId?>"><?= sizeof($post['Like']) ?></p>
              </li>
                <?php
                  $isLiked = false;
                  for ($i = 0; $i < sizeof($post['Like']); $i++) {
                    $isLiked = ($post['Like'][$i]['user_id'] == AuthComponent::user('id') ? true : false);
                  } ?>
                  <?php if ($isLiked) { ?>
                  <li class="btnLike">
                    <p style="color:#e44d3a !important" class="com comLabel commentLabel<?=$likeId?>" onclick="unlikePost(<?=$likeId?>);">
                      <i class="fa fa-thumbs-up"></i> Like
                    </p>
                  </li>
                  <?php } else { ?>
                    <li class="btnLike">
                    <p class="com comLabel commentLabel<?=$likeId?>" onclick="likePost(<?=$likeId?>);">
                      <i class="fa fa-thumbs-up"></i> Like
                    </p>
                  </li>
                  <?php } ?>
                <li>
                  <a title="" id="<?=$id?>" class="com commentShow" >
                    <?= sizeof($post['Comment'])?> <i class="fa fa-comment"></i>
                      Comment
                  </a>
                </li>
                <li>
                  <?= $this->Html->link(sizeof($post['NoOfRetweets']).' <i class="fa fa-retweet"></i> Retweet',
                  ['action' => 'retweet', $id],
                  ['escape' => false, 'class' => 'com']) ?>
                </li>
              </ul>
            </div>
            <!--/ #statusSection -->
          </div>
          <!--post-bar-->

          <div class="comment-section" style="padding-left:15px !important;">
            <?php
              $totalComments = sizeof($post['Comment']);
              if ($totalComments >= 3) {
                $limitcomments = $totalComments - 3;
                if ($totalComments > 3): ?>
                  <div class="plus-ic">
                    <?= $this->Html->Link('View All Comments',
                      ['controller' => 'posts',
                      'action' => 'view',
                      $id],
                      ['title' => 'View all comments', 'escape' => false])?>
                  </div>
                <?php endif; ?>
              <?php for ($i = $limitcomments; $i < $totalComments; $i++) {?>
                <div class="comment-sec<?=$id?>">
                  <ul>
                    <li>
                      <div id="commentSection" class="comment-list">
                        <div class="bg-img">
                          <?= $this->Html->image("profile/" . h($post['Comment'][$i]['User']['image']));?>
                        </div>
                        <div class="comment">
                          <h3>
                            <?php
                              $commentUserId = HashidsComponent::encode($post['Comment'][$i]['User']['id']);
                              echo $this->Html->link(h($post['Comment'][$i]['User']['first_name']) . " " . h($post['Comment'][$i]['User']['last_name']),
                                ['controller' => 'users',
                                'action' => 'view',
                                $commentUserId ]);
                              if (AuthComponent::user('id') == $post['Comment'][$i]['user_id']) {
                                $commentId = $post['Comment'][$i]['id'];?>
                                <i id="<?= $commentId ?>" data-id="<?= h($post['Comment'][$i]['content']);?>" class="editButton editBtn fa fa-edit"></i>
                                <?= $this->Html->link('<i class="fa fa-trash"></i>', [], [
                                    'escape' => false,
                                    'id' => $commentId,
                                    'class' => 'delButton delBtn']); ?>
                              <?php } ?>
                          </h3>
                          <span><i class="fa fa-clock-o"></i> <?= $this->Time->niceShort(h($post['Comment'][$i]['created'])) ?></span>
                          <div class="comment-list-content">
                            <p class="show-read-more">
                              <?= h($post['Comment'][$i]['content']); ?>
                            </p>
                          <div>
                        </div><!--/ comment -->
                      </div><!--/ comment-list end -->
                    </li>
                  </ul>
                </div><!--/ comment-sec end -->
              <?php
                } // end of for loop
              } ?>
            <?php
              // Do not show view all comments link
              if ($totalComments < 3) {
                for ($i = 0; $i < $totalComments; $i++) { ?>
                  <div class="comment-sec<?=$id?>">
                    <ul>
                      <li>
                        <div id="commentSection" class="comment-list">
                          <div class="bg-img">
                            <?= $this->Html->image("profile/" . h($post['Comment'][$i]['User']['image']));?>
                          </div>
                          <div class="comment">
                            <h3>
                              <?php
                              $commentUserId = HashidsComponent::encode($post['Comment'][$i]['User']['id']);
                              echo $this->Html->link(h($post['Comment'][$i]['User']['first_name']) . " " . h($post['Comment'][$i]['User']['last_name']), [
                                  'controller' => 'users',
                                  'action' => 'view',
                                  $commentUserId ]);

                              if (AuthComponent::user('id') == $post['Comment'][$i]['user_id']) {
                                $commentId = $post['Comment'][$i]['id'];?>
                                <i id="<?= $commentId ?>" data-id="<?= h($post['Comment'][$i]['content']);?>" class="editButton editBtn fa fa-edit"></i>
                                <?php
                                  echo $this->Html->link('<i class="fa fa-trash"></i>', [],
                                    ['escape' => false,
                                    'id' => $commentId,
                                    'class' => 'delButton delBtn']);?>
                              <?php } ?>
                            </h3>
                            <span> <i class="fa fa-clock-o"></i> <?= $this->Time->niceShort(h($post['Comment'][$i]['created']))?> </span>
                            <div class="comment-list-content">
                              <p> <?= h($post['Comment'][$i]['content'])?> </p>
                            <div>
                          </div>
                          <!--/ comment-->
                        </div>
                        <!--/ comment-list end-->
                      </li>
                    </ul>
                  </div>
                  <!--/ comment-sec end-->
                <?php
                }
              } ?>
            <div class="row post-comment" style="padding-bottom:30px;">
              <div class="bg-img">
                <?= $this->Html->image("profile/" . h($users['User']['image']));?>
              </div>
              <div class="comment_box">
                <?= $this->Form->create('Comment',
                  ['type' => 'post',
                  'id' => 'saveComment',
                  'class' => 'saveComment'])?>
                <?= $this->Form->input('postid',
                  ['type' => 'hidden',
                  'value' => $post['Post']['id'],
                  'label' => false])?>
                <?= $this->Form->input('userid',
                  ['type' => 'hidden',
                  'value' => AuthComponent::user('id'),
                  'label' => false])?>
                <?= $this->Form->input('content',
                  ['placeholder' => 'Post a comment',
                  'maxlength' => 140,
                  'label' => false])?>
              </div>
              <div>
                <?= $this->Form->input('Send',
                  ['type' => 'button',
                  'label' => false,
                  'id' => 'submitComment'.$post['Post']['id'],
                  'class'=>'comment_box2'])?>
                <?= $this->Form->end()?>
              </div>
            </div>
            <!--/ post-comment end-->
          </div>
          <!--/ comment-section-->
          <?php endforeach; ?>
        </div>
        <!--/ post-section -->
      </div>
      <!--/ main-ws-sec -->
      <div class="paging">
        <?php
          if (sizeof($posts) > 0) {
            echo $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled'));
            echo $this->Paginator->numbers(array('separator' => ''));
            echo $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled'));
          } else {
            echo 'No posts yet.';
          }
        ?>
      </div>
      <!--/ pagination -->
    </div>
    <!--/ col-lg-6 col-md-8 no-pd -->
  </div>
  <!--/ row -->
</div>
<!--/ main-section -->

<!-- View Following and Followers Profile -->
<!-- Modal -->
<div class="modal fade" id="viewFollowers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"> Followers </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </div>
      <!--/ modal-header -->
      <div class="modal-body">
        <div class="container-fluid">
          <?php if (!empty($followers)) { ?>
            <?php foreach ($followers as $follower): ?>
            <div class="post_topbar">
              <div class="usy-dt">
                <?= $this->Html->image("profile/" . h($follower['User']['image']));?>
                  <div class="usy-name">
                    <h3><?= h($follower['User']['first_name']). " " . h($follower['User']['last_name'])?></h3>
                  </div>
                  <!--/ usy-name -->
                </div>
                <!--/ usy-dt -->
                <div class="ed-user-list">
                  <?php $followerId = HashidsComponent::encode($follower['User']['id']);
                    echo $this->Html->Link('View Profile',
                        ['controller'=> 'users',
                        'action' => 'view',
                        $followerId],
                        ['escape' => false,
                        'class' => 'btn btn-primary']);?>
                </div>
                <!--/ ed-opts -->
              </div>
            </div>
            <?php endforeach; ?>
            <?php } else { ?>
              <div class="col-md-12">
                No Followers yet
              </div>
            <?php } ?>
        </div>
        <!--/ container-fluid -->
      </div>
      <!--/ modal body -->
    </div>
    <!--/ modal-content -->
  </div>
  <!--/ modal-dialog -->
</div>
<!--/ modal fade -->

<!-- View Following and Followers Profile -->
<!-- Modal -->
<div class="modal fade" id="viewFollowing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Following</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </div>
      <!--/ modal-header -->
      <div class="modal-body">
        <div class="container-fluid">
        <?php if (!empty($following)) { ?>
          <?php foreach ($following as $followings): ?>
            <div class="post_topbar">
              <div class="usy-dt">
                <?= $this->Html->image("profile/" . h($followings['User']['image']));?>
                  <div class="usy-name">
                    <h3><?= h($followings['User']['first_name']). " " . h($followings['User']['last_name'])?></h3>
                  </div>
                  <!--/ usy-name -->
                </div>
                <!--/ usy-dt -->
                <div class="ed-user-list">
                  <?php
                    $followingId = HashidsComponent::encode($followings['User']['id']);
                    echo $this->Html->Link('View Profile',
                      ['controller' => 'users',
                      'action' => 'view',
                      $followingId],
                      ['escape' => false,
                      'class' => 'btn btn-primary']);?>
                </div>
                <!--/ ed-opts -->
              </div>
            <?php endforeach; ?>
            <?php } else { ?>
              <div class="col-md-12">
                No followings yet
              </div>
            <?php } ?>
        </div>
        <!--/ container-fluid -->
      </div>
      <!--/ modal body -->
    </div>
    <!--/ modal-content -->
  </div>
  <!--/ modal-dialog -->
</div>
<!--/ modal fade -->

<!--View user profile image -->
<!-- Modal -->
<div class="modal fade" id="viewImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </div>
      <!--/ modal-header -->
      <div class="modal-body">
        <?= $this->Html->image("profile/" . h($users['User']['image']),
          ['style'=>'max-width:100%; max-height:100%;'])?>
      </div>
      <!--/ modal-body -->
    </div>
    <!--/ modal-content -->
  </div>
  <!--/ modal-dialog -->
</div>
<!--/ modal fade -->

<!-- Edit Comment Modal -->
<!-- Modal -->
<div class="modal fade" id="editComments" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title">Edit Comment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </div>
      <!--/ modal-header -->
      <div class="modal-body">
        <?= $this->Form->create('Comment',
            ['type' => 'get',
            'id' => 'editComment',
            'class' => 'editComment']);?>
        <?= $this->Form->input('postid',
            ['id' => 'commentId',
            'type' => 'hidden',
            'label' => false]);?>
        <div class="form-group">
          <?= $this->Form->input('content',
            ['type' => 'textarea',
            'rows' => '3',
            'cols' => '60',
            'id' => 'commentContent',
            'class' => 'form-control',
            'maxlength' => 140,
            'label' => false]);?>
            <small class="form-text text-muted">
              <span style="float:left;" id="commentRemainingCharacter">140</span>
            </small>
        </div>
        <div class="form-group">
          <?= $this->Form->input('Save Changes',
            ['type' => 'button',
            'label' => false,
            'id' => 'submitEditedComment',
            'class'=>'comment_box2'])?>
          <?= $this->Form->end() ?>
        </div>
      </div>
      <!--/ modal-body -->
    </div>
    <!--/ modal-content -->
  </div>
  <!--/ modal-dialog -->
</div>
<!--/ modal fade -->
<script>

$(document).ready(function () {
  $(".saveComment").submit(function(e){

    //prevent page from loading
    e.preventDefault();

    var formData = $(this).serializeArray();
    console.log(formData);
    dataObj = {};

    //get specific post id
    $(formData).each(function(i, field){
      dataObj[field.name] = field.value;
      commentPostId = dataObj["data[Comment][postid]"];
    });

    // disable send button after submit
    $("#submitComment" + commentPostId).attr("disabled", true);

    $.ajax({
      type: 'POST',
      url: "../../comments/add/",
      data: formData,
      success: function(data,textStatus,xhr){
        window.location.reload();
      },
      error: function(xhr,textStatus,error){
        window.location.reload();
      }
    });
    return false;
  });
});
</script>
