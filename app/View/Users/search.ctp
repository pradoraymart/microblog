<div class="main-section-data">
  <div class="row">
    <div class="col-lg-6 col-md-8 offset-md-3 no-pd">
      <div id="posts-list" class="main-ws-sec">
        <div class="posts-section">
        <h4> People </h4>
        <?php if(sizeOf($user) > 0) { ?>
          <?php foreach ($user as $users): ?>
            <div class="post-bar">
              <div class="post_topbar">
                <div class="usy-dt">
                  <?= $this->Html->image("profile/" . h($users['User']['image']));?>
                  <div class="usy-name">
                    <h3>
                      <?php
                        $id = HashidsComponent::encode($users['User']['id']);
                        echo h($users['User']['first_name']). " " . h($users['User']['last_name']);?>
                    </h3>
                    <small class="text-muted"><?= h($users['User']['username']);?></small>
                  </div>
                  <!--/ usy-name -->
                </div>
                <!--/ usy-dt -->
                <div class="ed-user-list">
                  <?= $this->Html->link('View Profile',
                      ['controller' => 'users',
                      'action' => 'view',
                      $id],
                      ['escape' => false,
                      'class' => 'btn btn-primary']
                    );?>
                </div>
                <!--/ ed-opts -->
              </div>
              <!--/ post_topbar -->
            </div>
            <!-- / post-bar -->
          <?php endforeach; ?>
          <?php } else { ?>
            <div class="row justify-content-center align-items-center">
              No Users Found
            </div>
          <?php } ?>
        </div>
        <!-- / post-section -->
        <h4> Posts </h4>
        <?php
          // set for user comment
          foreach ($authUser as $authUser):
          endforeach;
        ?>
        <?php if (sizeOf($posts) > 0) { ?>
        <?php foreach ($posts as $post): ?>
        <div class="post-bar">
          <div class="post_topbar">
            <div class="usy-dt">
              <?= $this->Html->image("profile/" . h($post['User']['image']));?>
              <div class="usy-name">
                <h3>
                  <?php
                    $userId = HashidsComponent::encode($post['User']['id']);
                    echo $this->Html->link(h($post['User']['first_name']). " " . h($post['User']['last_name']),
                      ['controller' => 'users',
                      'action' => 'view',
                      $userId]);
                  ?>
                </h3>
                <span>
                  <i class="fa fa-clock-o"></i>
                  <?= $this->Time->niceShort(h($post['Post']['created']));?>
                </span>
              </div>
              <!-- / usy-name -->
            </div>
            <!-- / usy-dt -->
            <div class="ed-opts">
              <?php
              $postId = HashidsComponent::encode($post['Post']['id']);
              if (AuthComponent::user('id') == $post['Post']['user_id']) {?>
                <?= $this->Html->link(
                  '<i class="fa fa-edit"></i>',
                  ['controller' => 'posts', 'action' => 'edit', $postId],
                  ['escape' => false, 'title' => __('Edit')]);?>
                <?= $this->Form->postLink(
                $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                    ['controller' => 'posts', 'action' => 'delete', $postId],
                    ['escape' => false],
                  __('Are you sure you want to delete?', $postId));?>
              <?php } ?>
            </div>
            <!-- / ed-opts -->
          </div>
          <!-- / post_topbar -->
          <?php if(isset($post['Post']['post_id'])) { ?>
            <div class="post_content"  onclick="window.location.href='<?= Router::url(['controller' => 'posts', 'action' => 'view', $postId]);?>'">
              <p> <?= h($post['Post']['content'])?> </p>
              <div class="retweet-bar col-md-11 offset-md-1">
                <div class="retweet_bar">
                  <div class="post_topbar">
                    <div class="usy-dt">
                      <?= $this->Html->image("profile/" . h($post['AuthorRetweet']['image']));?>
                      <div class="usy-name">
                        <h6>
                          <?php
                            $authorId = HashidsComponent::encode($post['AuthorRetweet']['id']);
                            echo $this->Html->link(h($post['AuthorRetweet']['first_name']). " " . h($post['AuthorRetweet']['last_name']),
                              ['controller' => 'users',
                              'action' => 'view',
                              $authorId]);
                          ?>
                        </h6>
                        <span>
                          <i class="fa fa-clock-o"></i>
                          <?= $this->Time->niceShort(h($post['Retweet']['created'])) ?>
                        </span>
                      </div>
                      <!-- / usy-name -->
                    </div>
                    <!-- / usy-dt -->
                  </div>
                  <!-- / post_topbar -->
                  <div class="post_content"  onclick="window.location.href='<?= Router::url(['controller' => 'posts', 'action' => 'view', $postId]);?>'">
                  <?php
                    // check post text and image presence
                    if (empty($post['Post']['image'])) {
                      // check the post origin if has been deleted
                      if ($post['Retweet']['is_deleted'] == 1) {
                        echo '<p style="color:#e44d3a;"> Content is not available</p>';
                      } else {
                        echo '<p>';
                          echo h($post['Post']['retweet_content']);
                        echo '</p>';
                      }
                    } else {
                      if ($post['Retweet']['is_deleted'] == 1) {
                        echo '<p style="color:#e44d3a;"> Content Not Available</p>';
                      } else {
                        echo '<p>';
                          echo h($post['Post']['retweet_content']);
                        echo '</p>';
                        echo $this->Html->image("posts/" . h($post['Post']['image']));
                      }
                    }?>
                  </div>
                  <!-- / post-content -->
                </div>
                <!-- / retweet-bar -->
              </div>
              <!--/ retweet-bar col-md-11 -->
            </div>
            <!--/ post content -->
          <?php } else { ?>
            <div class="post_content" onclick="window.location.href='<?= Router::url(['controller' => 'posts', 'action' => 'view', $postId]);?>'">
              <?php
                if (empty($post['Post']['image'])) {
                  echo '<p>';
                    echo h($post['Post']['content']);
                  echo '</p>';
                } else {
                  echo '<p>';
                    echo h($post['Post']['content']);
                  echo '</p>';
                  echo $this->Html->image("posts/" . h($post['Post']['image']));
                }?>
            </div>
            <!--/ post content -->
            <?php } ?>
            <div id="statusSection" class="job-status-bar">
              <ul class="like-com">
                <?php $likeId = HashidsComponent::decode($postId);?>
                <li style="margin-right: 0px !important;">
                  <p class="countLikes<?=$likeId?>"><?= sizeof($post['Like']);?></p>
                </li>
                <?php
                  $isLiked = false;
                  for ($i = 0; $i < sizeof($post['Like']); $i++) {
                    $isLiked = ($post['Like'][$i]['user_id'] == AuthComponent::user('id') ? true : false);
                  } ?>
                  <?php if ($isLiked) { ?>
                    <li class="btnLike">
                      <p style="color:#e44d3a !important" class="com comLabel commentLabel<?=$likeId?>" onclick="unlikePost(<?=$likeId?>);">
                        <i class="fa fa-thumbs-up"></i> Like
                      </p>
                    </li>
                  <?php } else { ?>
                  <li class="btnLike">
                    <p class="com comLabel commentLabel<?=$likeId?>" onclick="likePost(<?=$likeId?>);">
                      <i class="fa fa-thumbs-up"></i> Like
                    </p>
                  </li>
                <?php } ?>
                <li>
                  <a title="" id="<?=$postId?>" class="com commentShow"><i class="fa fa-comment"></i> <?= sizeof($post['Comment'])?> Comment </a>
                </li>
                <li>
                  <?= $this->Html->link(
                    '<i class="fa fa-retweet"></i> ' .sizeof($post['NoOfRetweets']). ' Retweet',
                    ['controller' => 'posts',
                    'action' => 'retweet',
                    $postId],
                    ['escape' => false,
                    'class' => 'com']);?>
                </li>
              </ul>
            </div>
            <!--/ #statusSection -->
          </div>
          <!--/ post-bar -->
          <div class="comment-section" style="padding-left:15px !important;">
            <?php
              $totalComments = sizeof($post['Comment']);
              if ($totalComments >= 3) {
                $limitcomments = $totalComments - 3;
                if ($totalComments > 3): ?>
                <div class="plus-ic">
                  <?= $this->Html->Link('View All Comments',
                    ['controller'=> 'posts',
                    'action' => 'view',
                    $postId],
                    ['title' => 'View all comments',
                    'escape' => false,]);?>
                </div>
              <?php
              endif;
              for ($i = $limitcomments; $i < $totalComments; $i++) {?>
                <div class="comment-sec<?=$postId ?>">
                  <ul>
                    <li>
                      <div id="commentSection" class="comment-list">
                        <div class="bg-img">
                          <?= $this->Html->image("profile/" . h($post['Comment'][$i]['User']['image']));?>
                        </div>
                        <div class="comment">
                          <h3>
                            <?php
                              $commentUserId = HashidsComponent::encode($post['Comment'][$i]['User']['id']);
                              echo $this->Html->link(h($post['Comment'][$i]['User']['first_name']) . " " . h($post['Comment'][$i]['User']['last_name']),
                                ['controller' => 'users',
                                'action' => 'view',
                                $commentUserId]);
                              if (AuthComponent::user('id') == $post['Comment'][$i]['user_id']) {
                                $commentId = $post['Comment'][$i]['id']; ?>
                                <i id="<?= $commentId ?>" data-id="<?= h($post['Comment'][$i]['content']);?>" class="editButton editBtn fa fa-edit"></i>
                                <?= $this->Html->link('<i class="fa fa-trash"></i>', [],
                                    ['escape' => false,
                                    'id' => $commentId,
                                    'class' => 'delButton delBtn']);
                              }?>
                          </h3>
                          <span><i class="fa fa-clock-o"></i> <?= $this->Time->niceShort(h($post['Comment'][$i]['created']))?></span>
                          <div class="comment-list-content">
                            <p> <?= h($post['Comment'][$i]['content'])?> </p>
                          <div>
                        </div>
                      </div><!--comment-list end-->
                    </li>
                  </ul>
                </div><!--comment-sec end-->
              <?php } ?>
            <?php } ?>
            <?php
             if ($totalComments < 3) {
                for ($i = 0; $i < $totalComments; $i++) {
                ?>
                <div class="comment-sec<?=$postId ?>">
                  <ul>
                    <li>
                      <div id="commentSection" class="comment-list">
                        <div class="bg-img">
                          <?= $this->Html->image("profile/" . h($post['Comment'][$i]['User']['image']));?>
                        </div>
                        <div class="comment">
                          <h3>
                            <?php
                              $commentUserId = HashidsComponent::encode($post['Comment'][$i]['User']['id']);
                              echo $this->Html->link(h($post['Comment'][$i]['User']['first_name']) . " " . h($post['Comment'][$i]['User']['last_name']),
                                ['controller' => 'users',
                                'action' => 'view',
                                $commentUserId]);
                              if (AuthComponent::user('id') == $post['Comment'][$i]['user_id']) {
                                $commentId = $post['Comment'][$i]['id'];?>
                                <i id="<?= $commentId ?>" data-id="<?= h($post['Comment'][$i]['content']);?>" class="editButton editBtn fa fa-edit"></i>
                                <?= $this->Html->link('<i class="fa fa-trash"></i>', [],
                                  ['escape' => false,
                                  'id' => $commentId,
                                  'class' => 'delButton delBtn']);
                              }?>
                          </h3>
                          <span> <i class="fa fa-clock-o"></i> <?= $this->Time->niceShort(h($post['Comment'][$i]['created']))?> </span>
                          <div class="comment-list-content">
                            <p> <?= h($post['Comment'][$i]['content'])?> </p>
                          <div>
                        </div>
                      </div><!--comment-list end-->
                    </li>
                  </ul>
                </div><!--comment-sec end-->
              <?php } ?>
            <?php } ?>
            <div class="row post-comment" style="padding-bottom:30px;">
              <div class="bg-img">
                <?= $this->Html->image("profile/" . h($authUser['User']['image']));?>
              </div>
              <div class="comment_box">
                <?= $this->Form->create('Comment',
                  ['id' => 'saveComment',
                  'class' => 'saveComment']);?>
                <?= $this->Form->input('postid',
                  ['type' => 'hidden',
                  'value' => $post['Post']['id'],
                  'label' => false]);?>
                <?= $this->Form->input('userid',
                  ['type' => 'hidden',
                  'value' => AuthComponent::user('id'),
                  'label' => false])?>
                <?= $this->Form->input('content',
                  ['placeholder' => 'Post a comment',
                  'maxlength' => 140,
                  'label' => false]);?>
              </div>
              <div>
                <?= $this->Form->input('Send',
                  ['type' => 'button',
                  'label' => false,
                  'id' => 'submitComment'.$post['Post']['id'],
                  'class'=>'comment_box2']);?>
                <?= $this->Form->end() ?>
              </div>
            </div>
            <!--/ post-comment end-->
          </div>
          <!--/ comment-section-->
        <?php endforeach; ?>
        <?php } else { ?>
          <div class="row justify-content-center align-items-center">
            No Posts Found
          </div>
        <?php } ?>
      </div>
      <!--/ main-ws-sec -->
      <div class="paging">
        <?php
          echo $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled'));
          echo $this->Paginator->numbers(array('separator' => ''));
          echo $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
      </div>
      <!--/ paging -->
    </div>
    <!--/ col-lg-6 col-md-8 no-pd -->
  </div>
  <!--/ row -->
</div>
<!--/ main-section -->

<!-- Edit Comment Modal -->
<!-- Modal -->
<div class="modal fade" id="editComments" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <h5 class="modal-title">Edit Comment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </div>
      <!--/ modal-header -->
      <div class="modal-body">
        <?= $this->Form->create('Comment',
            ['type' => 'get',
            'id' => 'editComment',
            'class' => 'editComment']);?>
        <?= $this->Form->input('postid',
          ['id' => 'commentId',
          'type' => 'hidden',
          'label' => false]);?>
        <div class="form-group">
          <?= $this->Form->input('content',
            ['type' => 'textarea',
            'rows' => '3',
            'cols' => '60',
            'id' => 'commentContent',
            'class' => 'form-control',
            'maxlength' => 140,
            'label' => false]);?>
            <small class="form-text text-muted">
              <span style="float:left;" id="commentRemainingCharacter">140</span>
            </small>
        </div>
        <div class="form-group">
          <?= $this->Form->input('Save Changes',
            ['type' => 'button',
            'label' => false,
            'id' => 'submitEditedComment',
            'class'=>'comment_box2']);?>
          <?= $this->Form->end() ?>
        </div>
      </div>
      <!--/ modal-body -->
    </div>
    <!--/ modal-content -->
  </div>
  <!--/ modal-dialog -->
</div>
<!--/ modal fade -->

<script type="text/javascript">
$(document).ready(function () {
  $(".saveComment").submit(function(e){
    //prevent page from loading
    e.preventDefault();

    var formData = $(this).serializeArray();
    console.log(formData);
    dataObj = {};

    $(formData).each(function(i, field){
      dataObj[field.name] = field.value;
      commentPostId = dataObj["data[Comment][postid]"];
    });

    //disable send button after submit
    $("#submitComment" + commentPostId).attr("disabled", true);

    $.ajax({
      type: 'POST',
      url: "../../comments/add/",
      data: formData,
      success: function(data,textStatus,xhr){
        window.location.reload();
      },
      error: function(xhr,textStatus,error){
        // show the response
        window.location.reload();
      }
    });
    return false;
  });
});
</script>
