<div class="row justify-content-center">
  <div class="col-md-6">
  <?php echo $this->Flash->render(); ?>
    <div class="card">
      <header class="card-header">
        <h4 class="card-title mt-2">Register</h4>
      </header>
      <div class="card-body">
        <?= $this->Form->create(['User']);?>
          <div class="form-row">
            <div class=" col form-group">
              <?= $this->Form->input('first_name', ['class' => 'form-control']); ?>
            </div> <!-- form-group end.// -->
            <div class=" col form-group">
              <?= $this->Form->input('last_name', ['class' => 'form-control']); ?>
            </div> <!-- form-group end.// -->
           </div><!-- form-row end.// -->
          <div class="form-group">
            <?= $this->Form->input('username', ['class' => 'form-control', 'required']); ?>
          </div> <!-- form-group end.// -->
          <div class="form-group">
            <?= $this->Form->input('email', ['class' => 'form-control', 'required']); ?>
            <small class="form-text text-muted">We'll never share your email with anyone else.</small>
          </div> <!-- form-group end.// -->
          <div class="form-row">
            <div class="col form-group">
              <label for="gender">Gender</label>
              <?php
                $options = ['0' => 'Male', '1' => 'Female'];
                echo $this->Form->input('gender',
                  ['type' => 'select',
                  'options' => $options,
                  'class' => 'form-control',
                  'label' => false]);
              ?>
            </div> <!-- form-group end.// -->
            <div class="col form-group" style="width:100%">
              <label for="date_of_birth">Date of Birth</label>
              <?= $this->Form->input('date_of_birth',
                ['type' => 'date',
                'label' => false,
                'class' => 'form-date',
                'minYear' => date('Y') - 70,
                'maxYear' => date('Y') - 0,
                'required']); ?>
            </div> <!-- form-group end.// -->
          </div> <!-- form-row end.// -->
          <div class="form-group">
            <?= $this->Form->input('password', ['class' => 'form-control', 'required']); ?>
          </div> <!-- form-group end.// -->
          <div class="form-group">
            <?= $this->Form->input('confirm_password',
              ['type' => 'password',
              'class' => 'form-control',
              'required']); ?>
          </div> <!-- form-group end.// -->
          <div class="form-group">
            <?= $this->Form->button('Register', ['action' => 'register', 'class'=>'btn btn-primary btn-block']);?>
          </div> <!-- form-group// -->
          <small class="text-muted">
            By clicking the 'Register' button, you confirm that you accept our <br> Terms of use and Privacy Policy.
          </small>
        <?= $this->Form->end(); ?>
      </div> <!-- card-body end .// -->
      <div class="border-top card-body text-center">Already have an account?
        <?= $this->Html->link('Login', ['action' => 'login']); ?>
      </div>
    </div> <!-- card.// -->
  </div> <!-- col.//-->
</div> <!-- row.//-->
