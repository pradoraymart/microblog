<br>
<?= $this->Flash->render(); ?>
<br>
<div class="row justify-content-center">
  <div class="col-md-4">
  <h3 style="text-align:center;color:#e44d3a;">MICROBLOG</h3><br>
    <div class="card">
      <header class="card-header">
        <h4 class="card-title mt-2">Log in</h4>
      </header>
      <div class="card-body">
        <?= $this->Form->create(['User']);?>
          <div class="form-group">
            <?= $this->Form->input('username', ['class' => 'form-control', 'required']); ?>
          </div> <!--/ form-group -->
          <div class="form-group">
            <?= $this->Form->input('password', ['class' => 'form-control', 'required']); ?>
          </div> <!--/ form-group end -->
            <?= $this->Form->button('Login', ['action' => 'login', 'class' => 'btn btn-primary btn-block']);?>
          </div> <!--/ form-group -->
        <?= $this->Form->end(); ?>
      </div> <!--/ card-body end -->
      <div class="border-top card-body text-center">Don't have account?
        <?= $this->Html->link('Register', ['action' => 'register']); ?>
      </div>
    </div> <!--/ card -->
  </div> <!--/ col -->
</div> <!--/ row -->
