<div class="main-section-data">
  <div class="row">
    <div class="col-lg-3 col-md-4 pd-left-none no-pd">
      <div class="main-left-sidebar no-margin">
        <div class="user-data full-width">
          <div class="user-profile">
            <div class="username-dt">
              <div class="usr-pic">
                <?= $this->Html->image("profile/" . h($user['User']['image']));?>
              </div>
            </div>
            <!--/ username-dt -->
            <div class="user-specs"></div>
          </div>
          <!--/ user-profile -->
        </div>
        <!--/ user-data -->
      </div>
      <!--/ main-left-sidebar -->
    </div>
    <!--/ main-left-sidebar -->
    <div class="col-lg-8 col-md-8 no-pd">
      <div class="main-ws-sec">
        <div class="posts-section">
          <div class="post-bar">
          <article class="card-body">
            <h2> Personal Information </h2>
            <hr>
              <?= $this->Form->create('User', ['enctype' => 'multipart/form-data']);?>
              <div class="form-group">
                <?= $this->Form->input('image', ['type' => 'file', 'label' => false]) ?>
              </div> <!-- form-group end.// -->
              <div class="form-row">
                <div class="col form-group">
                  <?= $this->Form->input('first_name', ['class' => 'form-control', 'required']); ?>
                </div> <!-- form-group end.// -->
                <div class="col form-group">
                  <?= $this->Form->input('last_name', ['class' => 'form-control', 'required']); ?>
                </div> <!-- form-group end.// -->
              </div> <!-- form-row end.// -->
              <div class="form-row">
                <div class="col form-group">
                <?php
                  $options = ['0' => 'Male', '1' => 'Female'];
                  echo $this->Form->input('gender',
                    ['type' => 'select',
                    'options' => $options,
                    'class' => 'form-control']);
                ?>
                </div> <!-- form-group end.// -->
              </div> <!-- form-row end.// -->
              <div class="form-group">
                <label for="self_description">About yourself</label>
                <?= $this->Form->input('self_description',
                  ['type' => 'textarea',
                  'rows' => '3',
                  'cols' => '60',
                  'maxlength' => 140,
                  'label' => false,
                  'class' => 'form-control',
                  'required' => false]); ?>
                <small class="form-text text-muted">Tell us more about yourself
                <span style="float:right;" id="remainingCharacter">140</span>
                </small>
              </div> <!-- form-group end.// -->

              <h2> Account Information </h2>
              <hr>
              <div class="form-group">
                <?= $this->Form->input('username',['class' => 'form-control', 'required']); ?>
              </div> <!-- form-group end.// -->
              <div class="form-group">
                <?= $this->Form->input('email',
                  ['placeholder' => h($user['User']['email']),
                  'class' => 'form-control',
                  'required',
                  'disabled']); ?>
              </div> <!-- form-group end.// -->
              <h4> Change Password </h4>
              <div class="form-group">
                <?= $this->Form->input('new_password',
                  ['label' => 'New Password',
                  'type' => 'password',
                  'class' => 'form-control']);
                ?>
              </div> <!-- form-group end.// -->
              <div class="form-group">
                <?= $this->Form->input('confirm_password',
                  ['label' => 'Confirm New Password',
                  'type' => 'password',
                  'class' => 'form-control']);
                ?>
              </div> <!-- form-group end.// -->
              <div class="form-group">
                <?= $this->Form->button('Save Changes',
                  ['action' => 'register',
                  'class'=>'btn btn-primary btn-block',
                  'onclick' => "this.form.submit(); this.disabled=true;"]);
                ?>
              </div> <!-- form-group// -->
              <?= $this->Form->end(); ?>
          </article> <!-- card-body end .// -->
          </div>
        </div> <!-- end of posts-section -->
      </div> <!-- end of main-ws-sec -->
    </div> <!-- col-lg-6 col-md-8 no-pd -->
  </div> <!-- end of row -->
</div> <!-- end of main-section -->
