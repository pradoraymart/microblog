<div class="row justify-content-center">
  <h2> You are almost there! </h2>
  <h6> A Verification link has been sent to your email <br> Please click the link to verify</h6>
  <br><br>
  <div>Already verified? Please Login here
    <?= $this->Html->link('Login', array('action' => 'login')); ?>
  </div>
</div>
